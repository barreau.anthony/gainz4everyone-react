# Gainz 4 everyone

Finally an app for all the gym bros out there, made for iOS and Android.

Keep track of your workouts and personal bests, start new programs, get friendly reminders of your schedule, etc !

![](doc/screenshot-1.png)
![](doc/screenshot-2.png)
![](doc/screenshot-3.png)
![](doc/screenshot-4.png)
![](doc/screenshot-5.png)


## What is this repository ?

This is a React app made with the Ionic Framework, based on the original one made with Angular, just for the sake of
learning a new framework.

## How do I install this wonderful app ?

Once you cloned the repository, type in your terminal :

```shell
npm ci && npm run start
```

The app should then be available through `http://localhost:3000`

## How can I fix a bug / contribute to it ?

I'm open to merge requests ! Feel free to open one, and I'll give you a hug once I'm rich !