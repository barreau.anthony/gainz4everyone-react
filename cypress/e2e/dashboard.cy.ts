describe('dashboard', () => {
  it('page-should-be-visible', () => {
    cy.viewport('samsung-s10');
    cy.visit('/');

    cy.get('h1.title').should('be.visible').contains('Welcome back!');

    cy.matchImageSnapshot({
      failureThreshold: 0.05, // threshold for entire image
      failureThresholdType: 'percent', // percent of image or number of pixels
    });
  })
})
