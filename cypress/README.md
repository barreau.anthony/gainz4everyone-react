# Cypress E2E tests

## Lancement des tests

### En local

Pour lancer les tests E2E en local, plusieurs choix s'offrent à vous mais le principe est toujours le même :
Il est nécessaire d'avoir un frontend de lancé et qui pointe vers un backend (local, test, qa, ...) et vous pouvez ensuite lancer les tests via une des commandes `npm` avec le préfixe `e2e`.

Commandes disponibles :

- `npm run e2e-local-qa` : fonctionne de paire avec la commande `npm run start`. Lance les tests E2E auprès d'un front qui doit tourner en local sur `http://localhost:4200`.
- `npm run e2e-ci` : prévu pour être lancé via la CI. Lance les tests E2E auprès d'un front qui doit tourner sur `http://gainz4everyone`. Voir le fichier `.gitlab-ci.yml` (job frontend:e2e) ainsi que `ci/docker/nginx-conf/ci/fft-ci.conf` et `ci/docker/docker-compose-ci.yml`. Si vous souhaitez lancer en local les tests e2e de la CI, voir le paragraphe plus bas.
- `npm run e2e-update-screenshots` : permet de mettre à jour l'ensemble des screenshots de référence pour les tests

### En local, via gitlab-runner-exec pour émuler la CI

Si vous avez besoin de pouvoir lancer sur votre machine le job frontend:e2e présent dans le fichier .gitlab-ci.yml, il est possible via la commande `ci frontend:e2e` dans le répertoire racine du projet FFT de démarrer le job.
Attention cependant, il sera probablement nécessaire pour vous de modifier la définition du job en ajoutant les variables suivantes :

```
variables:
  DOCKER_TLS_CERTDIR: "" # pour résoudre une erreur empéchant docker-compose de trouver le daemon docker 
  NEXUS_USERNAME: <votre_nom_dutilisateur>
  NEXUS_PASSWORD: <votre_mot_de_passe>
  NEXUS_URL: https://nexus3.itkweb.fr
```

Egalement, le job de ci va lancer le `ci/docker/docker-compose-ci.yml` qui va lui même charger des images du nexus. Assurez vous d'être authentifié sur le nexus via la commande `docker login https://nexus3.itkweb.fr`.


## Ajout d'un test

Lors de l'ajout d'un test dans le dossier `winfield-ng/cypress/e2e/` il vous sera nécessaire de créer une image de référence qui sera utilisée pour comparer le résultat de votre test. La création de votre image se fait via la commande `npm run e2e-update-screenshots`.

**ATTENTION** : l'exécution de cette commande va mettre à jour **l'ensemble** des images de référence de tous les tests E2E. Vous devez donc vous assurez en premier lieu que les autres tests existants sont fonctionnels. N'hésitez pas à regarder le diff git de votre repository avant de commiter votre résultat pour vous assurer que les images mises à jour sont **cohérentes**.

Une fois votre image de référence créée, vous avez la possibilité de vérifier que votre feature est fonctionnelle en appelant dans le code de votre test la fonction suivante : `cy.matchImageSnapshot();`

## Jeu de données de test

