#!/usr/bin/env bash

set +x

echo "Will run E2E test against $CYPRESS_BASE_URL url."
cd /app
npm ci --force

echo "Building frontend..."
CI=false npm run build
npm run e2e-ci
