import {
  type AllExercises,
  type AllSets,
  type RpeSet,
  type TMSet,
} from "../interfaces/wod";

export const getSetsWithoutWarmup = (sets: AllSets[]): AllSets[] =>
  sets.filter((it) => it.type !== "warmup");

export const isExerciseWithVariableReps = (exercise: AllExercises): boolean => {
  if (exercise.type === "rpe") {
    return (
      exercise.sets
        .filter((it): it is RpeSet => it.type === "rpe")
        .map((it) => it.reps)
        .reduce((prev, cur) => (prev === cur ? cur : -1)) === -1
    );
  } else {
    return (
      exercise.sets
        .filter((it): it is TMSet => it.type === "tm")
        .map((it) => it.reps)
        .reduce((prev, cur) => (prev === cur ? cur : -1)) === -1
    );
  }
};

export const isExerciseWithVariableWeights = (
  exercise: AllExercises,
): boolean => {
  if (exercise.type === "rpe") {
    return false;
  } else {
    return (
      exercise.sets
        .filter((it): it is TMSet => it.type === "tm")
        .map((it) => it.load)
        .reduce((prev, cur) => (prev === cur ? cur : -1)) === -1
    );
  }
};

export const extractRepsFromExercise = (
  exercise: AllExercises,
): string | number => {
  if (exercise.type === "rpe") {
    return exercise.sets
      .filter((it): it is RpeSet => it.type === "rpe")
      .map((it) => it.reps)[0];
  } else {
    return exercise.sets
      .filter((it): it is TMSet => it.type === "tm")
      .map((it) => it.reps)[0];
  }
};

export const extractWeightsFromExercise = (
  exercise: AllExercises,
): string | number => {
  if (exercise.type === "rpe") {
    return exercise.sets
      .filter((it): it is RpeSet => it.type === "rpe")
      .map((it) => it.rpe)[0];
  } else {
    return exercise.sets
      .filter((it): it is TMSet => it.type === "tm")
      .map((it) => it.load)[0];
  }
};
