import { type PerformedWod } from "../interfaces/performedWod";
import { type AllExercises } from "../interfaces/wod";

import { updatePerformedWod } from "./performedWod";

describe("PerformedWod", () => {
  it("should update corresponding sets", () => {
    const performedWod: PerformedWod = {
      exercises: [],
    };

    const baseExercise1: AllExercises = {
      type: "tm",
      id: 0,
      sets: [
        {
          type: "tm",
          load: 10,
          reps: 5,
        },
        {
          type: "tm",
          load: 12,
          reps: 6,
        },
      ],
      name: "Bench",
      rest: 20,
    };

    const baseExercise2: AllExercises = {
      type: "tm",
      id: 1,
      sets: [
        {
          type: "tm",
          load: 10,
          reps: 100,
        },
        {
          type: "tm",
          load: 10,
          reps: 100,
        },
        {
          type: "tm",
          load: 10,
          reps: 100,
        },
      ],
      name: "Squat",
      rest: 20,
    };

    const updated = updatePerformedWod(
      performedWod,
      baseExercise1,
      5,
      80,
      0,
    ) as PerformedWod;
    expect(updated).toEqual({
      exercises: [
        {
          baseExercise: baseExercise1,
          sets: [
            {
              load: 80,
              reps: 5,
            },
          ],
        },
      ],
    });

    const updated2 = updatePerformedWod(
      updated,
      baseExercise1,
      2,
      100,
      1,
    ) as PerformedWod;
    expect(updated2).toEqual({
      exercises: [
        {
          baseExercise: baseExercise1,
          sets: [
            {
              load: 80,
              reps: 5,
            },
            {
              load: 100,
              reps: 2,
            },
          ],
        },
      ],
    });

    const updated3 = updatePerformedWod(
      updated2,
      baseExercise1,
      1,
      50,
      0,
    ) as PerformedWod;
    expect(updated3).toEqual({
      exercises: [
        {
          baseExercise: baseExercise1,
          sets: [
            {
              load: 50,
              reps: 1,
            },
            {
              load: 100,
              reps: 2,
            },
          ],
        },
      ],
    });

    const updated4 = updatePerformedWod(
      updated3,
      baseExercise2,
      10,
      80,
      0,
    ) as PerformedWod;
    expect(updated4).toEqual({
      exercises: [
        {
          baseExercise: baseExercise1,
          sets: [
            {
              load: 50,
              reps: 1,
            },
            {
              load: 100,
              reps: 2,
            },
          ],
        },
        {
          baseExercise: baseExercise2,
          sets: [
            {
              load: 80,
              reps: 10,
            },
          ],
        },
      ],
    });

    const updated5 = updatePerformedWod(updated4, baseExercise2, 8, 80, 1);
    expect(updated5).toEqual({
      exercises: [
        {
          baseExercise: baseExercise1,
          sets: [
            {
              load: 50,
              reps: 1,
            },
            {
              load: 100,
              reps: 2,
            },
          ],
        },
        {
          baseExercise: baseExercise2,
          sets: [
            {
              load: 80,
              reps: 10,
            },
            {
              load: 80,
              reps: 8,
            },
          ],
        },
      ],
    });
  });
});
