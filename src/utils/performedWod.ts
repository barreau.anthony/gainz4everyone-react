import {
  type PerformedExercise,
  type PerformedSet,
  type PerformedWod,
} from "../interfaces/performedWod";
import { type AllExercises } from "../interfaces/wod";

import { hasValue } from "./typescript";

export const updatePerformedWod = (
  performedWod: PerformedWod,
  baseExercise: AllExercises,
  performedReps: number,
  load: number,
  setIndex: number,
): PerformedWod => {
  const exercise = performedWod.exercises.find(
    (exercise) => exercise.baseExercise.id === baseExercise.id,
  );
  if (hasValue(exercise)) {
    const updatedExercise: PerformedExercise = {
      ...exercise,
      sets: Object.values({
        ...exercise.sets,
        [setIndex]: {
          reps: performedReps,
          load,
        },
      }) as PerformedSet[],
    };
    return {
      ...performedWod,
      exercises: Object.values({
        ...performedWod.exercises,
        [baseExercise.id]: updatedExercise,
      }) as PerformedExercise[],
    };
  } else {
    const updatedExercise: PerformedExercise = {
      baseExercise,
      sets: [
        {
          load,
          reps: performedReps,
        },
      ],
    };

    return {
      ...performedWod,
      exercises: [...performedWod.exercises, updatedExercise],
    };
  }
};
