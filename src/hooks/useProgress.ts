import { useEffect, useState } from "react";

import { useFirebaseContext } from "../components/firestore/FirebaseContext";
import { fetchPrograms } from "../redux/programs/programs.actions";
import { fetchProgress } from "../redux/progress/progress.actions";
import { useAppDispatch } from "../redux/store";

export const useProgress = (): boolean => {
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useAppDispatch();
  const { db, currentUser } = useFirebaseContext();

  useEffect(() => {
    if (currentUser) {
      setIsLoading(true);
      dispatch(fetchProgress(currentUser, db))
        .then(() => dispatch(fetchPrograms(db)))
        .then(() => setIsLoading(false));
    }
  }, [currentUser, db, dispatch]);

  return isLoading;
};
