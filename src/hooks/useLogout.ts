import { useIonToast } from "@ionic/react";
import { useCallback } from "react";
import { useHistory } from "react-router";

import { useFirebaseContext } from "../components/firestore/FirebaseContext";

export const useLogout = (): (() => void) => {
  const { auth, isAuthenticated } = useFirebaseContext();
  const [presentToast] = useIonToast();
  const history = useHistory();

  return useCallback(() => {
    if (isAuthenticated) {
      auth
        .signOut()
        .then(() =>
          presentToast({
            message: "You correctly signed out!",
            color: "primary",
            duration: 3000,
          }),
        )
        .then(() => history.replace("/login"));
    }
  }, [auth, history, isAuthenticated, presentToast]);
};
