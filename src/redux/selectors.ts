import { type Selector } from "@reduxjs/toolkit";

import { activitySelectors } from "./activity/activity.selectors";
import { currentProgramSelectors } from "./currentProgram/currentProgram.selectors";
import { currentWorkoutSelectors } from "./currentWorkout/currentWorkout.selectors";
import { personalBestsSelectors } from "./personalBests/personalBests.selectors";
import { programsSelectors } from "./programs/programs.selectors";
import { progressSelectors } from "./progress/progress.selectors";
import { type RootState, type store } from "./store";

export type AppState = ReturnType<typeof store.getState>;

export type AppSelector<T> = Selector<AppState, T>;

export const selectors = {
  activity: activitySelectors((state: RootState) => state.activity),
  currentProgram: currentProgramSelectors(
    (state: RootState) => state.currentProgram,
  ),
  currentWorkout: currentWorkoutSelectors(
    (state: RootState) => state.currentWorkout,
  ),
  personalBests: personalBestsSelectors(
    (state: RootState) => state.personalBests,
  ),
  progress: progressSelectors((state: RootState) => state.progress),
  programs: programsSelectors((state: RootState) => state.programs),
};
