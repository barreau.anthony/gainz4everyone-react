import { type AppSelector } from "../selectors";

import { type CurrentProgramState } from "./currentProgram.slice";

interface CurrentProgramSelectors {
  selectCurrentProgram: AppSelector<CurrentProgramState>;
}

export const currentProgramSelectors = (
  slicer: AppSelector<CurrentProgramState>,
): CurrentProgramSelectors => ({
  selectCurrentProgram: slicer,
});
