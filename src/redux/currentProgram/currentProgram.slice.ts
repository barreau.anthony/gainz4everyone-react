import { type PayloadAction, createSlice } from "@reduxjs/toolkit";

import { type Program } from "../../interfaces/program";

export type CurrentProgramState = Program | null;

const INITIAL_STATE: CurrentProgramState = null as CurrentProgramState;

export const currentProgramSlice = createSlice({
  name: "currentProgram",
  initialState: INITIAL_STATE,
  reducers: {
    saveCurrentProgram: (_, action: PayloadAction<Program>) => action.payload,
    removeCurrentProgram: () => INITIAL_STATE,
  },
});

export const { saveCurrentProgram, removeCurrentProgram } =
  currentProgramSlice.actions;
