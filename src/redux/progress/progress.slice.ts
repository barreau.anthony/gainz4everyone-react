import { type PayloadAction, createSlice } from "@reduxjs/toolkit";

import { type Progress } from "../../interfaces/progress";

export type ProgressState = Progress | null;

const INITIAL_STATE: ProgressState = null as ProgressState;

export const progressSlice = createSlice({
  name: "progress",
  initialState: INITIAL_STATE,
  reducers: {
    setProgress: (_, action: PayloadAction<Progress | null>) => action.payload,
  },
});

export const { setProgress } = progressSlice.actions;
