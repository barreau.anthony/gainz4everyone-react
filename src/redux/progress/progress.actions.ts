import { type User } from "firebase/auth";
import {
  type Firestore,
  deleteDoc,
  doc,
  getDoc,
  setDoc,
} from "firebase/firestore";

import { type ProgressClient } from "../../client/interfaces/progress";
import { type Progress } from "../../interfaces/progress";
import { type AppDispatch } from "../store";

import { setProgress } from "./progress.slice";

export const fetchProgress =
  (user: User, db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> =>
    getDoc<ProgressClient, ProgressClient>(doc(db, "progress", user.uid)).then(
      (res) => {
        dispatch(setProgress(res.exists() ? res.data() : null));
      },
    );

export const saveProgress =
  (progress: Progress | null, user: User, db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> =>
    setDoc(doc(db, "progress", user.uid), progress).then(() => {
      dispatch(setProgress(progress));
    });

export const deleteProgress =
  (progressId: string, db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> =>
    deleteDoc(doc(db, "progress", progressId)).then(() => {
      dispatch(setProgress(null));
    });
