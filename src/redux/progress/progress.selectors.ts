import { createSelector } from "@reduxjs/toolkit";

import { type AppSelector } from "../selectors";

import { type ProgressState } from "./progress.slice";

interface ProgressSelectors {
  selectCurrentProgress: AppSelector<ProgressState>;
  selectCurrentWodId: AppSelector<number | undefined>;
  selectCurrentProgramId: AppSelector<string | undefined>;
  selectCurrentExerciseId: AppSelector<number | undefined>;
}

export const progressSelectors = (
  slicer: AppSelector<ProgressState>,
): ProgressSelectors => ({
  selectCurrentProgress: slicer,
  selectCurrentWodId: createSelector(slicer, (progress) =>
    progress ? progress.currentWodId : undefined,
  ),
  selectCurrentProgramId: createSelector(slicer, (progress) =>
    progress ? progress.currentProgramId : undefined,
  ),
  selectCurrentExerciseId: createSelector(slicer, (progress) =>
    progress ? progress.currentExerciseId : undefined,
  ),
});
