import { type PayloadAction, createSlice } from "@reduxjs/toolkit";

import { type Program } from "../../interfaces/program";

export type ProgramsState = Program[];

const INITIAL_STATE: ProgramsState = [];

export const programsSlice = createSlice({
  name: "programs",
  initialState: INITIAL_STATE,
  reducers: {
    setPrograms: (state, action: PayloadAction<Program[]>) => action.payload,
  },
});

export const { setPrograms } = programsSlice.actions;
