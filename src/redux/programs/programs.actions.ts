import { type User } from "firebase/auth";
import {
  type Firestore,
  collection,
  deleteDoc,
  doc,
  getDocs,
  setDoc,
} from "firebase/firestore";

import { type ProgramClient } from "../../client/interfaces/programs";
import { type Program } from "../../interfaces/program";
import { uuid } from "../../utils/uuid";
import { fetchProgress } from "../progress/progress.actions";
import { type AppDispatch } from "../store";

import { setPrograms } from "./programs.slice";

export const startProgram =
  (program: Program, user: User, db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> =>
    setDoc(doc(db, "progress", user.uid), {
      currentProgramId: program.id,
      currentWodId: 0,
      currentExerciseId: 0,
      wodStatus: "not_started",
    }).then(() => {
      dispatch(fetchProgress(user, db));
    });

export const stopProgram =
  (user: User, db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> =>
    deleteDoc(doc(db, "progress", user.uid)).then(() => {
      dispatch(fetchProgress(user, db));
    });

export const fetchPrograms =
  (db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> =>
    getDocs<ProgramClient, ProgramClient>(collection(db, "programs"))
      .then((result) => result.docs.map((doc) => doc.data()))
      .then((res) => {
        dispatch(setPrograms(res));
      });

export const saveProgram =
  (program: Omit<Program, "id">, db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> => {
    const id = uuid();

    return setDoc(doc(db, "programs", id), {
      ...program,
      id,
    }).then(() => {
      dispatch(fetchPrograms(db));
    });
  };
