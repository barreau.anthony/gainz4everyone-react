import { type AppSelector } from "../selectors";

import { type ProgramsState } from "./programs.slice";

interface ProgramsSelectors {
  selectAll: AppSelector<ProgramsState>;
}

export const programsSelectors = (
  slicer: AppSelector<ProgramsState>,
): ProgramsSelectors => ({
  selectAll: slicer,
});
