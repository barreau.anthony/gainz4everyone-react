import { type PayloadAction, createSlice } from "@reduxjs/toolkit";

import { type Exercise } from "./current-workout-slice";

export const ACTIVITY_KEY = "activity";

export type PerformedExercise = Exercise & {
  performed: {
    setName: string;
    reps: number;
  }[];
};

export interface Activity {
  date: string;
  name: string;
  performedExercises: PerformedExercise[];
}

export const activitySlice = createSlice({
  name: ACTIVITY_KEY,
  initialState: null as unknown as Activity[],
  reducers: {
    saveActivity: (state, action: PayloadAction<Activity[]>) => [
      ...action.payload,
    ],
  },
});

export const { saveActivity } = activitySlice.actions;
