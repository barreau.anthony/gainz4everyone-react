import { type PayloadAction, createSlice } from "@reduxjs/toolkit";

export const CURRENT_WORKOUT_KEY = "currentWorkout";

export type Wod = {
  id: number;
  name: string;
  exercises: (RpeExercise | TMExercise)[];
  status: "done" | "in_progress" | "not_started";
  backgroundImage?: string;
};

export type Exercise = {
  id: number;
  name: string;
  rest: number;
  status: "in_progress" | "done" | "not_started";
};

export type RpeExercise = Exercise & {
  type: "rpe";
  sets: Array<WarmupSet | RpeSet>;
};

export type TMExercise = Exercise & {
  type: "tm";
  sets: Array<WarmupSet | TMSet>;
};

export type Set = {
  performedReps?: number;
};

export type RpeSet = Set & {
  type: "rpe";
  reps: number | "failure";
  rpe: string;
};

export type WarmupSet = Set & {
  type: "warmup";
  description: string;
};

export type TMSet = Set & {
  type: "tm";
  reps: number;
  load: number;
};

export type WorkoutState = Wod | null;

export const currentWorkoutSlice = createSlice({
  name: CURRENT_WORKOUT_KEY,
  initialState: null as WorkoutState,
  reducers: {
    saveCurrentWorkout: (state, action: PayloadAction<Wod | undefined>) => {
      return action.payload
        ? {
            id: action.payload.id,
            name: action.payload.name,
            exercises: [...action.payload.exercises],
            status: action.payload.status,
            backgroundImage: action.payload.backgroundImage,
          }
        : undefined;
    },
    removeCurrentWorkout: () => null,
  },
});

export const { saveCurrentWorkout, removeCurrentWorkout } =
  currentWorkoutSlice.actions;
