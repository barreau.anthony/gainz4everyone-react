import { configureStore } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";

import { activitySlice } from "./activity/activity.slice";
import { currentProgramSlice } from "./currentProgram/currentProgram.slice";
import { currentWorkoutSlice } from "./currentWorkout/currentWorkout.slice";
import { personalBestsSlice } from "./personalBests/personalBests.slice";
import { programsSlice } from "./programs/programs.slice";
import { progressSlice } from "./progress/progress.slice";

export const store = configureStore({
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
  reducer: {
    personalBests: personalBestsSlice.reducer,
    currentWorkout: currentWorkoutSlice.reducer,
    currentProgram: currentProgramSlice.reducer,
    progress: progressSlice.reducer,
    activity: activitySlice.reducer,
    programs: programsSlice.reducer,
  },
  preloadedState: {},
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const useAppDispatch: () => AppDispatch = useDispatch;
