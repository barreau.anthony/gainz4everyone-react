import { type AppSelector } from "../selectors";

import { type ActivityState } from "./activity.slice";

interface ActivitySelectors {
  selectAll: AppSelector<ActivityState[]>;
}

export const activitySelectors = (
  slicer: AppSelector<ActivityState[]>,
): ActivitySelectors => ({
  selectAll: slicer,
});
