import {
  type Firestore,
  collection,
  deleteDoc,
  doc,
  getDocs,
  setDoc,
} from "firebase/firestore";

import { type AllActivity as AllActivityClient } from "../../client/interfaces/activity";
import {
  type ScheduledActivity,
  mapFromClient,
  mapToClient,
} from "../../interfaces/activity";
import { type FreeActivityValues } from "../../pages/free-activity/FreeActivity.utils";
import { uuid } from "../../utils/uuid";
import { type AppDispatch } from "../store";

import { removeActivity, saveActivity, setActivities } from "./activity.slice";

export const insertFreeActivity =
  (activity: FreeActivityValues, db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> => {
    const id = uuid();
    return setDoc(
      doc(db, "activities", id),
      mapToClient({ ...activity, id }),
    ).then(() => {
      dispatch(saveActivity({ ...activity, id }));
    });
  };

export const insertScheduledActivity =
  (activity: Omit<ScheduledActivity, "id">, db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> => {
    const id = uuid();
    return setDoc(
      doc(db, "activities", id),
      mapToClient({ ...activity, id }),
    ).then(() => {
      dispatch(saveActivity({ ...activity, id }));
    });
  };

export const fetchActivities =
  (db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> =>
    getDocs<AllActivityClient, AllActivityClient>(collection(db, "activities"))
      .then((result) => result.docs.map((doc) => mapFromClient(doc.data())))
      .then((res) => {
        dispatch(setActivities(res));
      });

export const deleteActivity =
  (activityId: string, db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> =>
    deleteDoc(doc(db, "activities", activityId)).then(() => {
      dispatch(removeActivity(activityId));
    });
