import { type PayloadAction, createSlice } from "@reduxjs/toolkit";

import { type AllActivity } from "../../interfaces/activity";

export type ActivityState = AllActivity;

const INITIAL_STATE: ActivityState[] = [];

export const activitySlice = createSlice({
  name: "activity",
  initialState: INITIAL_STATE,
  reducers: {
    saveActivity: (state, action: PayloadAction<ActivityState>) => [
      ...state,
      action.payload,
    ],
    removeActivity: (state, action: PayloadAction<string>) =>
      state.filter((it) => it.id !== action.payload),
    setActivities: (_, action: PayloadAction<AllActivity[]>) => action.payload,
  },
});

export const { saveActivity, setActivities, removeActivity } =
  activitySlice.actions;
