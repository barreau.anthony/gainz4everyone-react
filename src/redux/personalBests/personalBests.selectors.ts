import { createSelector } from "@reduxjs/toolkit";

import { type PersonalBest } from "../../interfaces/personalBests";
import { type AppSelector } from "../selectors";

import { type PersonalBestState } from "./personalBests.slice";

interface PersonalBestsSelectors {
  selectAll: AppSelector<PersonalBestState>;
  selectLatest: AppSelector<PersonalBest | undefined>;
}

export const personalBestsSelectors = (
  slicer: AppSelector<PersonalBestState>,
): PersonalBestsSelectors => ({
  selectAll: slicer,
  selectLatest: createSelector(slicer, (state) =>
    state.length > 0
      ? [...state].sort((a, b) => a.date.compareTo(b.date))[state.length - 1]
          ?.pbs
      : undefined,
  ),
});
