import { type PayloadAction, createSlice } from "@reduxjs/toolkit";

import { type RecordedPersonalBests } from "../../interfaces/personalBests";

export type PersonalBestState = RecordedPersonalBests[];

const INITIAL_STATE: PersonalBestState = [];

export const personalBestsSlice = createSlice({
  name: "personalBests",
  initialState: INITIAL_STATE,
  reducers: {
    setPersonalBests: (_, action: PayloadAction<PersonalBestState>) =>
      action.payload,
  },
});

export const { setPersonalBests } = personalBestsSlice.actions;
