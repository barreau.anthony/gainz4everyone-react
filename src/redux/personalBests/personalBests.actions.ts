import {
  type Firestore,
  collection,
  doc,
  getDocs,
  setDoc,
} from "firebase/firestore";

import { type RecordedPersonalBestsClient } from "../../client/interfaces/personalBests";
import {
  type RecordedPersonalBests,
  mapFromClient,
  mapToClient,
} from "../../interfaces/personalBests";
import { uuid } from "../../utils/uuid";
import { type AppDispatch } from "../store";

import { setPersonalBests } from "./personalBests.slice";

export const fetchPersonalBests =
  (db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> =>
    getDocs<RecordedPersonalBestsClient, RecordedPersonalBestsClient>(
      collection(db, "personalBests"),
    )
      .then((result) => result.docs.map((doc) => mapFromClient(doc.data())))
      .then((res) => {
        dispatch(setPersonalBests(res));
      });

export const insertPersonalBests =
  (pbs: RecordedPersonalBests, db: Firestore) =>
  (dispatch: AppDispatch): Promise<void> =>
    setDoc(doc(db, "personalBests", uuid()), mapToClient(pbs)).then(() =>
      dispatch(fetchPersonalBests(db)),
    );
