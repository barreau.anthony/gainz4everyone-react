import { type PayloadAction, createSlice } from "@reduxjs/toolkit";

export const PERSONAL_BEST_KEY = "personalBests";

export interface PersonalBestState {
  deadlift: number | undefined;
  ohp: number | undefined;
  bench: number | undefined;
  squat: number | undefined;
}

export const personalBestsSlice = createSlice({
  name: PERSONAL_BEST_KEY,
  initialState: {
    deadlift: undefined,
    ohp: undefined,
    bench: undefined,
    squat: undefined,
  } as PersonalBestState,
  reducers: {
    saveNewPersonalBests: (
      state,
      action: PayloadAction<PersonalBestState>,
    ) => ({
      deadlift: action.payload.deadlift,
      squat: action.payload.squat,
      bench: action.payload.bench,
      ohp: action.payload.ohp,
    }),
  },
});

export const { saveNewPersonalBests } = personalBestsSlice.actions;
