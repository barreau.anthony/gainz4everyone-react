import { type PayloadAction, createSlice } from "@reduxjs/toolkit";

import { type Wod } from "./current-workout-slice";

export const CURRENT_PROGRAM_KEY = "currentProgram";

export type Program = {
  id: number;
  name: string;
  author: string;
  description: string;
  schedule: Wod[];
  repeatable: boolean; // whether the program should start again when the last schedule is done
  backgroundImage?: string;
};

export type ProgramState = Program | null;

export const currentProgramSlice = createSlice({
  name: CURRENT_PROGRAM_KEY,
  initialState: null as ProgramState,
  reducers: {
    saveCurrentProgram: (state, action: PayloadAction<Program>) => {
      return !action.payload
        ? null
        : ({
            id: action.payload.id,
            name: action.payload.name,
            description: action.payload.description,
            author: action.payload.author,
            backgroundImage: action.payload.backgroundImage,
            schedule: [...action.payload.schedule],
          } as Program);
    },
    removeCurrentProgram: () => null,
  },
});

export const { saveCurrentProgram, removeCurrentProgram } =
  currentProgramSlice.actions;
