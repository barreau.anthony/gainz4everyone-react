import { type PayloadAction, createSlice } from "@reduxjs/toolkit";

import { type Wod } from "../../interfaces/wod";

export type CurrentWorkoutState = Wod | null;

const INITIAL_STATE: CurrentWorkoutState = null as CurrentWorkoutState;

export const currentWorkoutSlice = createSlice({
  name: "currentWorkout",
  initialState: INITIAL_STATE,
  reducers: {
    saveCurrentWorkout: (_, action: PayloadAction<Wod>) => action.payload,
    removeCurrentWorkout: () => INITIAL_STATE,
  },
});

export const { saveCurrentWorkout, removeCurrentWorkout } =
  currentWorkoutSlice.actions;
