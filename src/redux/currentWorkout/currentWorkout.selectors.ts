import { type AppSelector } from "../selectors";

import { type CurrentWorkoutState } from "./currentWorkout.slice";

interface CurrentWorkoutSelectors {
  selectCurrentWorkout: AppSelector<CurrentWorkoutState>;
}

export const currentWorkoutSelectors = (
  slicer: AppSelector<CurrentWorkoutState>,
): CurrentWorkoutSelectors => ({
  selectCurrentWorkout: slicer,
});
