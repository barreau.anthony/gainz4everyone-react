import classNames from "classnames";
import React, { type PropsWithChildren, type ReactElement } from "react";

import styles from "./Card.module.css";

export interface CardProps {
  className?: string;
  backgroundImage?: string;
  size?: "small" | "regular";
}

const Card = ({
  className,
  backgroundImage,
  size = "regular",
  children,
}: PropsWithChildren<CardProps>): ReactElement => (
  <div
    className={classNames(className, styles.card, {
      [styles.shadowed]: backgroundImage,
    })}
  >
    {backgroundImage && (
      <div
        style={{
          backgroundImage: backgroundImage
            ? `url(assets/${backgroundImage})`
            : "",
        }}
        className={styles.background}
      />
    )}
    <div className={classNames(styles.content, styles[size])}>{children}</div>
  </div>
);

export default Card;
