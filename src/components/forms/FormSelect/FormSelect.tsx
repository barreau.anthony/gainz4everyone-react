import { IonNote, IonSelect, IonSelectOption } from "@ionic/react";
import { useField } from "formik";
import React, { type ComponentProps, type ReactElement } from "react";

import styles from "./FormSelect.module.css";

export interface FormSelectProps extends ComponentProps<typeof IonSelect> {
  name: string;
  options: {
    label: string;
    value: string;
  }[];
}

const FormSelect = ({ name, ...props }: FormSelectProps): ReactElement => {
  const [field, meta, helpers] = useField(name);
  return (
    <div className={styles.container}>
      <IonSelect
        {...field}
        {...props}
        label={props.label}
        className={`${meta.touched && "ion-touched"} ${meta.error && "ion-invalid"}`}
        errorText={meta.error}
        onIonBlur={() => helpers.setTouched(true, true)}
        onIonChange={(e) => helpers.setValue(e.detail.value, true)}
        name={name}
      >
        {props.options.map(({ label, value }, index) => (
          <IonSelectOption key={index} value={value}>
            {label}
          </IonSelectOption>
        ))}
      </IonSelect>
      {meta.error && (
        <IonNote className={styles.note} color="danger">
          {meta.error}
        </IonNote>
      )}
    </div>
  );
};

export default FormSelect;
