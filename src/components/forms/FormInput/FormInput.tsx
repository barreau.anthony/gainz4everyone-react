import { IonInput } from "@ionic/react";
import { useField } from "formik";
import React, { type ComponentProps, type ReactElement } from "react";

export interface FormInputProps extends ComponentProps<typeof IonInput> {
  name: string;
}

const FormInput = (props: FormInputProps): ReactElement => {
  const [field, meta, helpers] = useField(props.name);

  return (
    <IonInput
      {...field}
      {...props}
      label={props.label}
      className={`${meta.touched && "ion-touched"} ${meta.error && "ion-invalid"}`}
      value={field.value}
      name={props.name}
      errorText={meta.error}
      onIonBlur={() => helpers.setTouched(true, true)}
      onIonChange={(e) => helpers.setValue(e.detail.value, true)}
    />
  );
};

export default FormInput;
