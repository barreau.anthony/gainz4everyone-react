import React, { type PropsWithChildren, type ReactElement } from "react";

import { useField } from "../hooks/useField";

import styles from "./FormHelper.module.css";

export interface FormHelperProps {
  name: string;
  ignoreTouched?: boolean;
}

const FormHelper = ({
  ignoreTouched,
  name,
}: PropsWithChildren<FormHelperProps>): ReactElement | null => {
  const [, { showError, error }] = useField(name, { ignoreTouched });

  if (showError) {
    return <div className={styles.error}>{error}</div>;
  } else {
    return null;
  }
};

export default FormHelper;
