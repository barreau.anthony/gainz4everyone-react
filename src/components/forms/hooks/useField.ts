import {
  type FieldHelperProps,
  type FieldInputProps,
  type FieldMetaProps,
  useField as useFormikField,
} from "formik";
import { useMemo } from "react";

export interface FieldOptions {
  ignoreTouched?: boolean;
}

export const useField = <Field>(
  props: Parameters<typeof useFormikField<Field>>[0],
  { ignoreTouched = false }: FieldOptions = {},
): [
  FieldInputProps<Field>,
  FieldMetaProps<Field> & { showError: boolean },
  FieldHelperProps<Field>,
] => {
  const [values, meta, helpers] = useFormikField(props);

  return useMemo(() => {
    if ((meta.touched || ignoreTouched) && meta.error !== undefined) {
      return [values, { ...meta, showError: true }, helpers];
    } else {
      return [values, { ...meta, showError: false }, helpers];
    }
  }, [helpers, ignoreTouched, meta, values]);
};
