import { IonTextarea } from "@ionic/react";
import { useField } from "formik";
import React, { type ComponentProps, type ReactElement } from "react";

export interface FormTextareaProps extends ComponentProps<typeof IonTextarea> {
  name: string;
}

const FormTextarea = ({
  autoGrow = true,
  ...props
}: FormTextareaProps): ReactElement => {
  const [field, meta, helpers] = useField(props.name);

  return (
    <IonTextarea
      {...field}
      {...props}
      label={props.label}
      className={`${meta.touched && "ion-touched"} ${meta.error && "ion-invalid"}`}
      value={field.value}
      autoGrow={autoGrow}
      name={props.name}
      errorText={meta.error}
      onIonBlur={() => helpers.setTouched(true, true)}
      onIonChange={(e) => helpers.setValue(e.detail.value, true)}
    />
  );
};

export default FormTextarea;
