import { type FirebaseApp, initializeApp } from "firebase/app";
import {
  type Auth,
  type User,
  getAuth,
  onAuthStateChanged,
} from "firebase/auth";
import { type Firestore, getFirestore } from "firebase/firestore";
import React, {
  type PropsWithChildren,
  type ReactElement,
  createContext,
  useContext,
  useState,
} from "react";

const firebaseConfig = {
  apiKey: "AIzaSyAqwgCDjF5Vuj03i0am8ybRWXMv8gjlCFo",
  authDomain: "gainz-4-everyone.firebaseapp.com",
  projectId: "gainz-4-everyone",
  storageBucket: "gainz-4-everyone.appspot.com",
  messagingSenderId: "751882959224",
  appId: "1:751882959224:web:d9c18327aae6ecc2da708d",
};
const app = initializeApp(firebaseConfig);

export type FirebaseContextType = {
  app: FirebaseApp;
  db: Firestore;
  auth: Auth;
  currentUser: User | null;
  setCurrentUser: (user: User | null) => void;
  isAuthenticated: boolean;
  isAppReady: boolean;
};

export const FirebaseContext = createContext<FirebaseContextType>({
  app,
  db: getFirestore(app),
  auth: getAuth(app),
  currentUser: null,
  setCurrentUser: () => {},
  isAuthenticated: false,
  isAppReady: false,
});

export const FirebaseProvider = ({
  children,
}: PropsWithChildren): ReactElement => {
  const auth = getAuth(app);
  const [currentUser, setCurrentUser] = useState<User | null>(auth.currentUser);
  const [isAppReady, setIsAppReady] = useState(false);

  const firestoreCtx: FirebaseContextType = {
    app,
    db: getFirestore(app),
    auth,
    currentUser,
    setCurrentUser,
    isAuthenticated: currentUser !== null && isAppReady,
    isAppReady,
  };

  onAuthStateChanged(firestoreCtx.auth, (user) => {
    setCurrentUser(user);
    setIsAppReady(true);
  });

  return (
    <FirebaseContext.Provider value={firestoreCtx}>
      {children}
    </FirebaseContext.Provider>
  );
};

export const useFirebaseContext = (): FirebaseContextType => {
  const context = useContext(FirebaseContext);
  if (!context) {
    throw new Error(
      "You're trying to use a firebase context without being a children of a firebase context provider.",
    );
  } else {
    return context;
  }
};
