import { useIonToast } from "@ionic/react";
import React, { type ReactElement, useEffect } from "react";

import styles from "./OfflineBanner.module.css";

const OfflineBanner = (): ReactElement | null => {
  const [isOnline, setIsOnline] = React.useState(window.navigator.onLine);
  const [presentToast] = useIonToast();

  useEffect(() => {
    window.addEventListener("offline", () => setIsOnline(false));
    window.addEventListener("online", () => {
      setIsOnline(true);
      presentToast({
        message: "You're back online! Reloading the app...",
        color: "secondary",
        duration: 3000,
        onDidDismiss: () => window.location.reload(),
      });
    });
  }, [presentToast]);
  return !isOnline ? (
    <div className={styles.container}>
      Your internet connection appears to be down.
    </div>
  ) : null;
};

export default OfflineBanner;
