import { IonItem } from "@ionic/react";
import React, { type PropsWithChildren, type ReactElement } from "react";

import styles from "./Accordion.module.css";

const AccordionHeader = ({ children }: PropsWithChildren): ReactElement => (
  <IonItem className={styles.item} slot="header">
    <div className={styles.header}>{children}</div>
  </IonItem>
);

export default AccordionHeader;
