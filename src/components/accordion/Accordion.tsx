import { IonAccordion, IonAccordionGroup } from "@ionic/react";
import React, { type PropsWithChildren, type ReactElement } from "react";

import Card from "../card";

import styles from "./Accordion.module.css";

const Accordion = ({ children }: PropsWithChildren): ReactElement => (
  <Card>
    <IonAccordionGroup>
      <IonAccordion className={styles.accordion}>{children}</IonAccordion>
    </IonAccordionGroup>
  </Card>
);

export default Accordion;
