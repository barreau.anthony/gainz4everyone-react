import React, { type PropsWithChildren, type ReactElement } from "react";

import styles from "./Accordion.module.css";

const AccordionBody = ({ children }: PropsWithChildren): ReactElement => (
  <div className={styles.content} slot="content">
    {children}
  </div>
);

export default AccordionBody;
