import React, { type PropsWithChildren, type ReactElement } from "react";

import styles from "./Subtitle.module.css";

const Subtitle = ({ children }: PropsWithChildren): ReactElement => (
  <h2 className={styles.subtitle}>{children}</h2>
);

export default Subtitle;
