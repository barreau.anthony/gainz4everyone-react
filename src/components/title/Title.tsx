import React, { type PropsWithChildren, type ReactElement } from "react";

import styles from "./Title.module.css";

const Title = ({ children }: PropsWithChildren): ReactElement => (
  <h1 className={styles.title}>{children}</h1>
);

export default Title;
