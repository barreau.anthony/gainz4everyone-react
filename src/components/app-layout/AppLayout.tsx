import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonNav,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React, { type ReactElement } from "react";

import AppRouter from "../app-router/AppRouter";
import { useFirebaseContext } from "../firestore/FirebaseContext";
import Menu from "../menu/Menu";
import OfflineBanner from "../offline-banner";

import styles from "./AppLayout.module.css";

const PageLayout = (): ReactElement => {
  const { isAuthenticated } = useFirebaseContext();

  return (
    <>
      {isAuthenticated && <Menu />}
      <IonPage id="main-content">
        <IonHeader>
          <IonToolbar className={styles.toolbar}>
            <IonButtons slot="start">
              {isAuthenticated && <IonMenuButton></IonMenuButton>}
            </IonButtons>
            <IonTitle>Gainz 4 everyone</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className={styles.container}>
          <AppRouter />
        </IonContent>
        <OfflineBanner />
      </IonPage>
    </>
  );
};

const AppLayout = (): ReactElement => (
  <>
    <Menu />
    <IonNav root={() => <PageLayout />} />
  </>
);

export default AppLayout;
