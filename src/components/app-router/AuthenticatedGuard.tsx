import React, { type PropsWithChildren, type ReactElement } from "react";
import { Redirect } from "react-router";

import { useFirebaseContext } from "../firestore/FirebaseContext";

const AuthenticatedGuard = ({ children }: PropsWithChildren): ReactElement => {
  const { isAuthenticated } = useFirebaseContext();

  if (isAuthenticated) {
    return <>{children}</>;
  } else {
    return <Redirect to="/login" />;
  }
};

export default AuthenticatedGuard;
