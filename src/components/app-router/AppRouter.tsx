import { IonRouterOutlet } from "@ionic/react";
import React, { type ReactElement } from "react";
import { Redirect } from "react-router";
import { Route } from "react-router-dom";

import ActivityHistory from "../../pages/activity-history";
import DashboardPage from "../../pages/dashboard";
import FreeActivity from "../../pages/free-activity";
import Login from "../../pages/login";
import OneRms from "../../pages/onerms";
import Programs from "../../pages/programs";
import Wod from "../../pages/wod";
import WodSummary from "../../pages/wod-summary";

import AuthenticatedGuard from "./AuthenticatedGuard";

const AppRouter = (): ReactElement => (
  <IonRouterOutlet>
    <AuthenticatedGuard>
      <Route exact path="/dashboard">
        <DashboardPage />
      </Route>
      <Route exact path="/onerms">
        <OneRms />
      </Route>
      <Route exact path="/programs">
        <Programs />
      </Route>
      <Route exact path="/activity">
        <ActivityHistory />
      </Route>
      <Route exact path="/wod-summary">
        <WodSummary />
      </Route>
      <Route exact path="/wod">
        <Wod />
      </Route>
      <Route exact path="/free-activity">
        <FreeActivity />
      </Route>
      <Route exact path="/">
        <Redirect to="/dashboard" />
      </Route>
    </AuthenticatedGuard>
    <Route exact path="/login">
      <Login />
    </Route>
  </IonRouterOutlet>
);

export default AppRouter;
