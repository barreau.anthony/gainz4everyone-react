import { IonIcon, IonRippleEffect } from "@ionic/react";
import React, { type ReactElement } from "react";

import styles from "./CircularButton.module.css";

export interface CircularButtonProps {
  icon: string;
  onClick: () => void;
}

const CircularButton = ({
  icon,
  onClick,
}: CircularButtonProps): ReactElement => (
  <div
    onClick={onClick}
    className={`btn btn-floating btn-circular btn-primary ion-activatable ripple-parent circle ${styles.circularBtn}`}
  >
    <IonRippleEffect></IonRippleEffect>
    <IonIcon className={styles.icon} icon={icon}></IonIcon>
  </div>
);

export default CircularButton;
