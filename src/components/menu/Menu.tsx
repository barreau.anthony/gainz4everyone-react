import {
  IonContent,
  IonHeader,
  IonMenu,
  IonMenuToggle,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import {
  addOutline,
  appsOutline,
  barbellOutline,
  bookOutline,
  logOutOutline,
  newspaperOutline,
  trophyOutline,
} from "ionicons/icons";
import React, { type ReactElement, useMemo } from "react";
import { useSelector } from "react-redux";

import { useLogout } from "../../hooks/useLogout";
import { type Progress } from "../../interfaces/progress";
import { selectors } from "../../redux/selectors";

import MenuItem from "./MenuItem";

import styles from "./Menu.module.css";

const getWodMenuItem = (progress: Progress | null): ReactElement | null =>
  progress ? (
    <IonMenuToggle>
      {progress.wodStatus === "not_started" ? (
        <MenuItem label="WOD" path="/wod-summary" icon={barbellOutline} />
      ) : (
        <MenuItem label="WOD" path="/wod" icon={barbellOutline} />
      )}
    </IonMenuToggle>
  ) : null;
const Menu = (): ReactElement => {
  const progress = useSelector(selectors.progress.selectCurrentProgress);

  const wodMenuItem = useMemo(() => getWodMenuItem(progress), [progress]);
  const logout = useLogout();

  return (
    <IonMenu contentId="main-content">
      <IonHeader>
        <IonToolbar className={styles.toolbar}>
          <IonTitle>Gainz for everyone</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonMenuToggle>
          <MenuItem label="Dashboard" path="/dashboard" icon={appsOutline} />
        </IonMenuToggle>
        {wodMenuItem}
        <IonMenuToggle>
          <MenuItem
            label="Add free activity"
            path="/free-activity"
            icon={addOutline}
          />
        </IonMenuToggle>
        <IonMenuToggle>
          <MenuItem
            label="Available programs"
            path="/programs"
            icon={bookOutline}
          />
        </IonMenuToggle>
        <IonMenuToggle>
          <MenuItem
            label="Activity history"
            path="/activity"
            icon={newspaperOutline}
          />
        </IonMenuToggle>
        <IonMenuToggle>
          <MenuItem label="1 RMs" path="/onerms" icon={trophyOutline} />
        </IonMenuToggle>
        <IonMenuToggle>
          <MenuItem label="Logout" onClick={logout} icon={logOutOutline} />
        </IonMenuToggle>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
