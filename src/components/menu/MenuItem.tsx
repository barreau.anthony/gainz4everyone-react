import { IonIcon, IonItem } from "@ionic/react";
import classNames from "classnames";
import React, { type ReactElement } from "react";
import { useHistory, useLocation } from "react-router";

import { hasValue } from "../../utils/typescript";

import styles from "./Menu.module.css";

export interface MenuItemProps {
  path?: string;
  onClick?: () => void;
  icon: string;
  label: string;
}

const MenuItem = ({
  label,
  path,
  icon,
  onClick,
}: MenuItemProps): ReactElement => {
  const history = useHistory();
  const location = useLocation();

  return (
    <IonItem
      onClick={hasValue(path) ? (): void => history.push(path) : onClick}
      className={classNames(
        styles.item,
        path === location.pathname ? styles.active : undefined,
      )}
    >
      <IonIcon className={styles.icon} icon={icon} />
      {label}
    </IonItem>
  );
};

export default MenuItem;
