// eslint-disable-next-line import/order
import "@js-joda/timezone";

// eslint-disable-next-line import/order
import React from "react";

import { createRoot } from "react-dom/client";

import App from "./App";

const container = document.getElementById("root");
const root = createRoot(container!);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);
