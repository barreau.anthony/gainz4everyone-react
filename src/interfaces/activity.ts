import { DateTimeFormatter, ZonedDateTime } from "@js-joda/core";

import { type AllActivity as AllActivityClient } from "../client/interfaces/activity";

import type { PerformedWod } from "./performedWod";

export interface Activity {
  id: string;
  date: ZonedDateTime;
  title: string;
  description: string;
}

export interface FreeActivity extends Activity {
  type: "free";
}

export interface ScheduledActivity extends Activity {
  type: "scheduled";
  performedWod: PerformedWod;
}

export type AllActivity = FreeActivity | ScheduledActivity;

export const mapToClient = ({
  date,
  ...props
}: AllActivity): AllActivityClient => ({
  date: date.format(DateTimeFormatter.ISO_ZONED_DATE_TIME),
  ...props,
});

export const mapFromClient = ({
  date,
  ...props
}: AllActivityClient): AllActivity => ({
  date: ZonedDateTime.parse(date, DateTimeFormatter.ISO_ZONED_DATE_TIME),
  ...props,
});
