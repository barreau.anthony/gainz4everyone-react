import { type AllExercises } from "./wod";

export interface PerformedWod {
  exercises: PerformedExercise[];
}

export interface PerformedExercise {
  baseExercise: AllExercises;
  sets: PerformedSet[];
}

export interface PerformedSet {
  reps: number;
  load: number;
}
