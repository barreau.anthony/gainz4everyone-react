import { type Wod } from "./wod";

export interface Program {
  id: string;
  name: string;
  author: string;
  description: string;
  schedule: Wod[];
  repeatable: boolean; // whether the program should start again when the last schedule is done
  backgroundImage?: string;
}
