export interface Progress {
  currentProgramId: string;
  currentWodId: number;
  currentExerciseId: number;
  wodStatus: "not_started" | "started" | "finished";
}
