import { DateTimeFormatter, ZonedDateTime } from "@js-joda/core";

import { type RecordedPersonalBestsClient } from "../client/interfaces/personalBests";

export interface PersonalBest {
  bench: number;
  ohp: number;
  deadlift: number;
  squat: number;
}

export interface RecordedPersonalBests {
  date: ZonedDateTime;
  pbs: PersonalBest;
}

export const mapToClient = ({
  date,
  pbs,
}: RecordedPersonalBests): RecordedPersonalBestsClient => ({
  date: date.format(DateTimeFormatter.ISO_ZONED_DATE_TIME),
  pbs,
});

export const mapFromClient = ({
  date,
  pbs,
}: RecordedPersonalBestsClient): RecordedPersonalBests => ({
  date: ZonedDateTime.parse(date, DateTimeFormatter.ISO_ZONED_DATE_TIME),
  pbs,
});
