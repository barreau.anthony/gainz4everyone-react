export type Wod = {
  id: number;
  name: string;
  exercises: AllExercises[];
  backgroundImage?: string;
};

export type AllExercises = RpeExercise | TMExercise;

export type AllSets = WarmupSet | RpeSet | TMSet;

export interface BaseExercise {
  id: number;
  name: string;
  rest: number;
  sets: AllSets[];
}

export interface RpeExercise extends BaseExercise {
  type: "rpe";
}

export interface TMExercise extends BaseExercise {
  type: "tm";
}

export interface Set {
  performedReps?: number;
}

export interface RpeSet extends Set {
  type: "rpe";
  reps: number | "failure";
  rpe: string;
}

export interface WarmupSet extends Set {
  type: "warmup";
  description: string;
}

export interface TMSet extends Set {
  type: "tm";
  reps: number;
  load: number;
}
