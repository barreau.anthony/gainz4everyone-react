import { type PerformedWod } from "../../interfaces/performedWod";

export interface Activity {
  id: string;
  date: string;
  title: string;
  description: string;
}

export interface FreeActivity extends Activity {
  type: "free";
}

export interface ScheduledActivity extends Activity {
  type: "scheduled";
  performedWod: PerformedWod;
}

export type AllActivity = FreeActivity | ScheduledActivity;
