export type Wod = {
  id: number;
  name: string;
  exercises: AllExercises[];
  backgroundImage?: string;
};

export type AllExercises = RpeExercise | TMExercise;

export type AllSets = WarmupSet | RpeSet | TMSet;

export interface BaseExercise {
  id: number;
  name: string;
  rest: number;
  sets: AllSets[];
}

export interface RpeExercise extends BaseExercise {
  type: "rpe";
}

export interface TMExercise extends BaseExercise {
  type: "tm";
}

export interface Set {
  performedReps?: number;
}

export interface RpeSet extends Set {
  type: "rpe";
  reps: number | "failure";
  rpe: string;
}

export interface WarmupSet extends Set {
  type: "warmup";
  description: string;
}

export interface TMSet extends Set {
  type: "tm";
  reps: number;
  load: number;
}

export interface ProgramClient {
  id: string;
  name: string;
  author: string;
  description: string;
  schedule: Wod[];
  repeatable: boolean; // whether the program should start again when the last schedule is done
  backgroundImage?: string;
}
