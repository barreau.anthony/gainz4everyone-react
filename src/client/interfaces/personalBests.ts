export interface PersonalBestClient {
  bench: number;
  ohp: number;
  deadlift: number;
  squat: number;
}

export interface RecordedPersonalBestsClient {
  date: string;
  pbs: PersonalBestClient;
}
