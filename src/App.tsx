import { IonApp, setupIonicReact } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import React, { type ReactElement } from "react";
import { Provider as ReduxProvider } from "react-redux";

import AppLayout from "./components/app-layout";
import { FirebaseProvider } from "./components/firestore/FirebaseContext";
import { store } from "./redux/store";
import "./theme/app.css";

setupIonicReact();

const App = (): ReactElement => (
  <FirebaseProvider>
    <ReduxProvider store={store}>
      <IonApp>
        <IonReactRouter>
          <AppLayout />
        </IonReactRouter>
      </IonApp>
    </ReduxProvider>
  </FirebaseProvider>
);

export default App;
