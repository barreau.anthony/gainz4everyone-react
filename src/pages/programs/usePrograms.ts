import { useEffect, useState } from "react";

import { useFirebaseContext } from "../../components/firestore/FirebaseContext";
import { fetchPrograms } from "../../redux/programs/programs.actions";
import { useAppDispatch } from "../../redux/store";

export const usePrograms = (): boolean => {
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useAppDispatch();
  const { db } = useFirebaseContext();

  useEffect(() => {
    setIsLoading(true);
    dispatch(fetchPrograms(db)).then(() => setIsLoading(false));
  }, [db, dispatch]);

  return isLoading;
};
