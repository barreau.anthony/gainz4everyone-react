import { IonIcon } from "@ionic/react";
import { pulse } from "ionicons/icons";
import React, { type ReactElement, memo } from "react";

import { type AllExercises } from "../../../../interfaces/wod";
import {
  extractRepsFromExercise,
  isExerciseWithVariableReps,
} from "../../../../utils/exercises";

export interface RepCellProps {
  exercise: AllExercises;
}

const RepCell = ({ exercise }: RepCellProps): ReactElement =>
  isExerciseWithVariableReps(exercise) ? (
    <IonIcon name={pulse} />
  ) : (
    <>{extractRepsFromExercise(exercise)}</>
  );

export default memo(RepCell);
