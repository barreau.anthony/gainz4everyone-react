import {
  IonAccordion,
  IonAccordionGroup,
  IonItem,
  IonLabel,
} from "@ionic/react";
import React, { type ReactElement } from "react";

import Card from "../../../../components/card";
import { type Wod } from "../../../../interfaces/wod";
import { getSetsWithoutWarmup } from "../../../../utils/exercises";

import RepCell from "./RepCell";
import styles from "./Schedule.module.css";
import WeightCell from "./WeightCell";

export interface ScheduleProps {
  schedule: Wod[];
}

const Schedule = ({ schedule }: ScheduleProps): ReactElement => (
  <>
    {schedule.map((wod) => (
      <Card key={`wod-${wod.id}`} size="small">
        <IonAccordionGroup>
          <IonAccordion className={styles.accordion}>
            <IonItem className={styles.item} slot="header">
              <IonLabel>{wod.name}</IonLabel>
            </IonItem>
            <div slot="content" className={styles.content}>
              <table className={styles.table}>
                <thead>
                  <tr>
                    <td></td>
                    <td className={styles.columnHeader}>SETS</td>
                    <td className={styles.columnHeader}>REPS</td>
                    <td className={styles.columnHeader}>WEIGHT</td>
                    <td className={styles.columnHeader}>REST</td>
                  </tr>
                </thead>
                <tbody>
                  {wod.exercises.map((exercise, index) => (
                    <tr key={index}>
                      <td className={styles.exerciseName}>{exercise.name}</td>
                      <td>{getSetsWithoutWarmup(exercise.sets).length}</td>
                      <td>
                        <RepCell exercise={exercise} />
                      </td>
                      <td>
                        <WeightCell exercise={exercise} />
                      </td>
                      <td>{exercise.rest}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </IonAccordion>
        </IonAccordionGroup>
      </Card>
    ))}
  </>
);

export default Schedule;
