import { IonIcon } from "@ionic/react";
import { pulse } from "ionicons/icons";
import React, { type ReactElement, memo } from "react";

import { type AllExercises } from "../../../../interfaces/wod";
import {
  extractWeightsFromExercise,
  isExerciseWithVariableWeights,
} from "../../../../utils/exercises";

export interface WeightCellProps {
  exercise: AllExercises;
}

const WeightCell = ({ exercise }: WeightCellProps): ReactElement =>
  isExerciseWithVariableWeights(exercise) ? (
    <IonIcon name={pulse} />
  ) : (
    <>
      {exercise.type === "rpe" && "RPE "}
      {extractWeightsFromExercise(exercise)}
    </>
  );

export default memo(WeightCell);
