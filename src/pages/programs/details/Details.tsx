import {
  IonBackButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React, { type ReactElement } from "react";

import Subtitle from "../../../components/subtitle";
import Title from "../../../components/title";
import { type Program } from "../../../interfaces/program";

import styles from "./Details.module.css";
import Schedule from "./schedule";

export interface DetailsProps {
  program: Program;
}

const Details = ({ program }: DetailsProps): ReactElement => (
  <>
    <IonHeader className="header">
      <IonToolbar>
        <IonButtons slot="start">
          <IonBackButton></IonBackButton>
        </IonButtons>
        <IonTitle>Program details</IonTitle>
      </IonToolbar>
    </IonHeader>
    <IonContent
      fullscreen
      className={`main ion-padding ${styles.content}`}
      style={{ backgroundImage: `url('assets/${program.backgroundImage}')` }}
    >
      <Title>{program.name}</Title>
      <Subtitle>by {program.author}</Subtitle>
      <p>{program.description}</p>
      <Schedule schedule={program.schedule} />
    </IonContent>
  </>
);

export default Details;
