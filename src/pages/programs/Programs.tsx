import React, { type ReactElement } from "react";

import ProgramsList from "./ProgramsList";

const Programs = (): ReactElement => <ProgramsList />;

export default Programs;
