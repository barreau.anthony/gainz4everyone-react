import {
  IonButton,
  IonContent,
  IonNavLink,
  IonSpinner,
  useIonAlert,
} from "@ionic/react";
import { createSelector } from "@reduxjs/toolkit";
import React, { type ReactElement, useCallback } from "react";
import { useSelector } from "react-redux";

import Card from "../../components/card";
import { useFirebaseContext } from "../../components/firestore/FirebaseContext";
import Title from "../../components/title";
import { useProgress } from "../../hooks/useProgress";
import { type Program } from "../../interfaces/program";
import {
  startProgram,
  stopProgram,
} from "../../redux/programs/programs.actions";
import { selectors } from "../../redux/selectors";
import { useAppDispatch } from "../../redux/store";

import Details from "./details";
import { usePrograms } from "./usePrograms";

import styles from "./Programs.module.css";

const selectProgramsListData = createSelector(
  selectors.progress.selectCurrentProgramId,
  selectors.programs.selectAll,
  (currentProgramId, availablePrograms) => ({
    currentProgramId,
    availablePrograms,
  }),
);

const ProgramsList = (): ReactElement => {
  useProgress();

  const { currentProgramId, availablePrograms } = useSelector(
    selectProgramsListData,
  );

  const { db, currentUser } = useFirebaseContext();
  const dispatch = useAppDispatch();

  const loading = usePrograms();

  const [presentAlert] = useIonAlert();

  const handleStartProgramClick = useCallback(
    (program: Program) => {
      if (currentUser) {
        dispatch(startProgram(program, currentUser, db));
      }
    },
    [currentUser, db, dispatch],
  );

  const handleStopProgramClick = useCallback(() => {
    presentAlert({
      message: "Are your sure you want to stop this program?",
      buttons: [
        {
          text: "Ok",
          role: "confirm",
        },
        {
          text: "Cancel",
          role: "cancel",
        },
      ],
      onDidDismiss: ({ detail }) => {
        if (detail.role === "confirm" && currentUser) {
          dispatch(stopProgram(currentUser, db));
        }
      },
    });
  }, [currentUser, db, dispatch, presentAlert]);

  return (
    <IonContent fullscreen className="main ion-padding">
      <Title>Available programs</Title>
      {loading ? (
        <IonSpinner />
      ) : availablePrograms.length > 0 ? (
        availablePrograms.map((program) => (
          <Card key={program.id} backgroundImage={program.backgroundImage}>
            <span className={styles.programTitle}>{program.name}</span>
            <span className={styles.programAuthor}>by {program.author}</span>
            <div className={styles.buttonsWrapper}>
              <IonNavLink
                routerDirection="forward"
                component={() => <Details program={program} />}
              >
                <IonButton color="dark">Details</IonButton>
              </IonNavLink>
              {currentProgramId === program.id ? (
                <IonButton color="danger" onClick={handleStopProgramClick}>
                  Stop
                </IonButton>
              ) : (
                <IonButton onClick={() => handleStartProgramClick(program)}>
                  Start
                </IonButton>
              )}
            </div>
          </Card>
        ))
      ) : (
        <Card>There is no available program.</Card>
      )}
    </IonContent>
  );
};

export default ProgramsList;
