import { number, object } from "yup";

import { type PersonalBest } from "../../interfaces/personalBests";

export interface OneRmsValues {
  bench: number;
  ohp: number;
  deadlift: number;
  squat: number;
}

export const ONE_RMS_INITIAL_VALUES: OneRmsValues = {
  bench: 0,
  ohp: 0,
  deadlift: 0,
  squat: 0,
};

export const getInitialValues = (
  pbs: PersonalBest = ONE_RMS_INITIAL_VALUES,
): OneRmsValues => pbs;

export const ONE_RMS_SCHEMA = object().shape({
  bench: number().min(0),
  ohp: number().min(0),
  deadlift: number().min(0),
  squat: number().min(0),
});
