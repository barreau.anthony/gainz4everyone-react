import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import { useFirebaseContext } from "../../components/firestore/FirebaseContext";
import { fetchPersonalBests } from "../../redux/personalBests/personalBests.actions";
import { selectors } from "../../redux/selectors";
import { useAppDispatch } from "../../redux/store";

export const usePersonalBests = (): boolean => {
  const personalBests = useSelector(selectors.personalBests.selectLatest);
  const dispatch = useAppDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const { db } = useFirebaseContext();

  useEffect(() => {
    if (!personalBests) {
      setIsLoading(true);
      dispatch(fetchPersonalBests(db)).then(() => setIsLoading(false));
    }
  }, [db, dispatch, personalBests]);

  return isLoading;
};
