import { IonFab, IonFabButton, IonIcon } from "@ionic/react";
import { useFormikContext } from "formik";
import { save } from "ionicons/icons";
import React, { type ReactElement } from "react";

import FormInput from "../../components/forms/FormInput";

const OneRmsForm = (): ReactElement => {
  const { submitForm } = useFormikContext();
  return (
    <>
      <FormInput name="bench" label="Bench" labelPlacement="floating" />
      <FormInput name="deadlift" label="Deadlift" labelPlacement="floating" />
      <FormInput name="ohp" label="OHP" labelPlacement="floating" />
      <FormInput name="squat" label="Squat" labelPlacement="floating" />
      <IonFab slot="fixed" vertical="bottom" horizontal="end">
        <IonFabButton onClick={submitForm}>
          <IonIcon icon={save}></IonIcon>
        </IonFabButton>
      </IonFab>
    </>
  );
};

export default OneRmsForm;
