import { IonContent, IonSpinner, useIonToast } from "@ionic/react";
import { ZonedDateTime } from "@js-joda/core";
import { Formik } from "formik";
import React, { type ReactElement, useCallback, useMemo } from "react";
import { useSelector } from "react-redux";

import { useFirebaseContext } from "../../components/firestore/FirebaseContext";
import Subtitle from "../../components/subtitle";
import Title from "../../components/title";
import { insertPersonalBests } from "../../redux/personalBests/personalBests.actions";
import { selectors } from "../../redux/selectors";
import { useAppDispatch } from "../../redux/store";

import {
  ONE_RMS_SCHEMA,
  type OneRmsValues,
  getInitialValues,
} from "./OneRms.utils";
import OneRmsForm from "./OneRmsForm";
import { usePersonalBests } from "./usePersonalBest";

const OneRms = (): ReactElement => {
  const personalBests = useSelector(selectors.personalBests.selectLatest);
  const dispatch = useAppDispatch();
  const loading = usePersonalBests();

  const initialValues = useMemo(
    () => getInitialValues(personalBests),
    [personalBests],
  );

  const [presentToast] = useIonToast();

  const { db } = useFirebaseContext();
  const handleSubmit = useCallback(
    (values: OneRmsValues) => {
      if (db) {
        dispatch(
          insertPersonalBests(
            {
              date: ZonedDateTime.now(),
              pbs: values,
            },
            db,
          ),
        ).then(() =>
          presentToast({
            message: "Personal bests saved!",
            duration: 1000,
            color: "success",
            position: "bottom",
          }),
        );
      }
    },
    [db, dispatch, presentToast],
  );

  return (
    <IonContent fullscreen className="main ion-padding onerms">
      <Title>Progress</Title>
      <Subtitle>1 RMs</Subtitle>

      {!loading ? (
        <Formik
          initialValues={initialValues}
          validationSchema={ONE_RMS_SCHEMA}
          onSubmit={handleSubmit}
        >
          <OneRmsForm />
        </Formik>
      ) : (
        <IonSpinner />
      )}
    </IonContent>
  );
};

export default OneRms;
