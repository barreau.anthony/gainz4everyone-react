import { ZonedDateTime } from "@js-joda/core";
import * as yup from "yup";

export const ACTIVITY_TYPES = [
  {
    value: "Cross Training",
    label: "Cross Training",
  },
  {
    value: "Weight lifting",
    label: "Weight lifting",
  },
  {
    value: "Cardio",
    label: "Cardio",
  },
] as const;

export interface FreeActivityValues {
  title: (typeof ACTIVITY_TYPES)[number]["value"];
  type: "free";
  description: string;
  date: ZonedDateTime;
}

export const FREE_ACTIVITY_SCHEMA = yup.object({
  date: yup.object().required("You must set a date"),
  title: yup.string().required("You must specify an activity."),
  description: yup.string().required("You must put a description."),
});

export const FREE_ACTIVITY_INITIAL_VALUES: FreeActivityValues = {
  title: "Weight lifting",
  type: "free",
  description: "",
  date: ZonedDateTime.now(),
};
