import { type IonDatetimeCustomEvent } from "@ionic/core/dist/types/components";
import {
  type DatetimeChangeEventDetail,
  IonDatetime,
  IonDatetimeButton,
  IonFab,
  IonFabButton,
  IonIcon,
  IonItem,
  IonLabel,
  IonModal,
} from "@ionic/react";
import { nativeJs } from "@js-joda/core";
import { useFormikContext } from "formik";
import { save } from "ionicons/icons";
import React, { type ReactElement, useCallback } from "react";

import FormSelect from "../../../components/forms/FormSelect";
import FormTextarea from "../../../components/forms/FormTextarea";
import { ACTIVITY_TYPES } from "../FreeActivity.utils";

import styles from "./ActivityForm.module.css";

const ActivityForm = (): ReactElement => {
  const { submitForm, setFieldValue } = useFormikContext();
  const handleDateTimeChange = useCallback(
    (e: IonDatetimeCustomEvent<DatetimeChangeEventDetail>) => {
      if (typeof e.detail.value === "string") {
        setFieldValue("date", nativeJs(new Date(Date.parse(e.detail.value))));
      }
    },
    [setFieldValue],
  );

  return (
    <>
      <div className={styles.container}>
        <IonItem>
          <IonLabel>Date</IonLabel>
          <IonDatetimeButton datetime="datetime" />
          <IonModal keepContentsMounted>
            <IonDatetime
              onIonChange={handleDateTimeChange}
              id="datetime"
            ></IonDatetime>
          </IonModal>
        </IonItem>
        <IonItem>
          <FormSelect
            labelPlacement="floating"
            interface="action-sheet"
            label="Activity type"
            name="title"
            options={ACTIVITY_TYPES}
          />
        </IonItem>
        <IonItem>
          <FormTextarea
            labelPlacement="floating"
            label="Description"
            name="description"
          />
        </IonItem>
      </div>
      <IonFab slot="fixed" vertical="bottom" horizontal="end">
        <IonFabButton onClick={submitForm}>
          <IonIcon icon={save} />
        </IonFabButton>
      </IonFab>
    </>
  );
};

export default ActivityForm;
