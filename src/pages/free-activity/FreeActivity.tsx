import { IonContent, useIonToast } from "@ionic/react";
import { Formik } from "formik";
import React, { type ReactElement, useCallback } from "react";

import { useFirebaseContext } from "../../components/firestore/FirebaseContext";
import Title from "../../components/title";
import { insertFreeActivity } from "../../redux/activity/activity.actions";
import { useAppDispatch } from "../../redux/store";

import ActivityForm from "./Form";
import {
  FREE_ACTIVITY_INITIAL_VALUES,
  FREE_ACTIVITY_SCHEMA,
  type FreeActivityValues,
} from "./FreeActivity.utils";

const FreeActivity = (): ReactElement => {
  const dispatch = useAppDispatch();
  const [present] = useIonToast();
  const { db } = useFirebaseContext();
  const submit = useCallback(
    (values: FreeActivityValues) => {
      if (db) {
        dispatch(insertFreeActivity(values, db)).then(() =>
          present({
            message: "Activity saved!",
            duration: 1000,
            color: "primary",
            position: "bottom",
          }),
        );
      }
    },
    [db, dispatch, present],
  );

  return (
    <IonContent fullscreen className="main ion-padding">
      <Formik
        onSubmit={submit}
        initialValues={FREE_ACTIVITY_INITIAL_VALUES}
        validationSchema={FREE_ACTIVITY_SCHEMA}
      >
        <>
          <Title>Add a free activity</Title>
          <ActivityForm />
        </>
      </Formik>
    </IonContent>
  );
};

export default FreeActivity;
