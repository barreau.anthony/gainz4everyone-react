import React, { type ReactElement } from "react";

export interface FreeActivityProps {
  description: string;
}

const FreeActivity = ({ description }: FreeActivityProps): ReactElement => (
  <div>{description}</div>
);

export default FreeActivity;
