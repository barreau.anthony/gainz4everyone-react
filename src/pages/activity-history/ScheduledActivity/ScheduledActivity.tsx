import classNames from "classnames";
import React, { type ReactElement } from "react";

import { type PerformedWod } from "../../../interfaces/performedWod";

import styles from "./ScheduledActivity.module.css";

export interface ScheduledActivityProps {
  description: string;
  performedWod: PerformedWod;
}

const ScheduledActivity = ({
  performedWod,
  description,
}: ScheduledActivityProps): ReactElement => (
  <div className={styles.container}>
    <span className={styles.description}>{description}</span>
    {performedWod.exercises.map((exercise, index) => (
      <div
        className={classNames(styles.exerciseWrapper, {
          [styles.alt]: index % 2 === 0,
        })}
        key={exercise.baseExercise.id}
      >
        <div className={styles.exerciseName}>{exercise.baseExercise.name}</div>
        <div className={styles.setsWrapper}>
          {exercise.sets.map((set, index) => (
            <div
              key={`${exercise.baseExercise.id}-set-${index}`}
              className={styles.set}
            >
              <span>{set.reps} reps</span>
              <span>@</span>
              <span>{set.load}kgs</span>
            </div>
          ))}
        </div>
      </div>
    ))}
  </div>
);

export default ScheduledActivity;
