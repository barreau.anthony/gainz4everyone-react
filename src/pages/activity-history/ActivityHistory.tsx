import {
  IonButton,
  IonContent,
  IonIcon,
  useIonAlert,
  useIonToast,
} from "@ionic/react";
import { DateTimeFormatter } from "@js-joda/core";
import { trash } from "ionicons/icons";
import React, {
  type ReactElement,
  useCallback,
  useEffect,
  useMemo,
} from "react";
import { useSelector } from "react-redux";

import Accordion from "../../components/accordion";
import AccordionBody from "../../components/accordion/AccordionBody";
import AccordionHeader from "../../components/accordion/AccordionHeader";
import Card from "../../components/card";
import { useFirebaseContext } from "../../components/firestore/FirebaseContext";
import Title from "../../components/title";
import {
  deleteActivity,
  fetchActivities,
} from "../../redux/activity/activity.actions";
import { selectors } from "../../redux/selectors";
import { useAppDispatch } from "../../redux/store";

import ScheduledActivity from "./ScheduledActivity";

import styles from "./ActivityHistory.module.css";

const ActivityHistory = (): ReactElement => {
  const activityHistory = useSelector(selectors.activity.selectAll);

  const sortedActivities = useMemo(
    () => activityHistory.toSorted((a, b) => b.date.compareTo(a.date)),
    [activityHistory],
  );

  const { db } = useFirebaseContext();
  const dispatch = useAppDispatch();
  const [presentAlert] = useIonAlert();
  const [presentToast] = useIonToast();

  useEffect(() => {
    if (db) {
      dispatch(fetchActivities(db));
    }
  }, [db, dispatch]);

  const handleClickDelete = useCallback(
    (id: string) => {
      presentAlert({
        message: "Are your sure you want to delete this activity?",
        buttons: [
          {
            text: "Ok",
            role: "confirm",
          },
          {
            text: "Cancel",
            role: "cancel",
          },
        ],
        onDidDismiss: ({ detail }) => {
          if (detail.role === "confirm") {
            dispatch(deleteActivity(id, db)).then(() => {
              presentToast({
                message: "You correctly deleted your activity!",
                color: "primary",
                duration: 3000,
              });
            });
          }
        },
      });
    },
    [db, dispatch, presentAlert, presentToast],
  );

  return (
    <IonContent
      fullscreen
      className={`main ion-padding ${styles.activityPage}`}
    >
      <Title>Activity</Title>

      {sortedActivities.length === 0 ? (
        <Card>
          Your workout history is empty! Why don&apos;t you try one of our
          program or maybe do a free activity?
        </Card>
      ) : (
        sortedActivities.map((activity, index) => (
          <Accordion key={index}>
            <AccordionHeader>
              <div className={styles.activityHeader}>
                <div className={styles.dateContainer}>
                  <div className={styles.day}>
                    {activity.date.format(DateTimeFormatter.ofPattern("dd/MM"))}
                  </div>
                  <div className={styles.year}>
                    {activity.date.format(DateTimeFormatter.ofPattern("yyyy"))}
                  </div>
                </div>
                <div className={styles.type}>{activity.title}</div>
              </div>
            </AccordionHeader>
            <AccordionBody>
              {activity.type === "scheduled" ? (
                <ScheduledActivity
                  description={activity.description}
                  performedWod={activity.performedWod}
                />
              ) : (
                <div>{activity.description}</div>
              )}
              <div className={styles.actionsWrapper}>
                <IonButton
                  onClick={() => handleClickDelete(activity.id)}
                  color="danger"
                  size="small"
                >
                  <IonIcon slot="icon-only" ios={trash} md={trash} />
                </IonButton>
              </div>
            </AccordionBody>
          </Accordion>
        ))
      )}
    </IonContent>
  );
};

export default ActivityHistory;
