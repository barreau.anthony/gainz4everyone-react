import { IonContent } from "@ionic/react";
import React, { type ReactElement } from "react";

import Subtitle from "../../components/subtitle";
import Title from "../../components/title";
import { hasValue } from "../../utils/typescript";

import { WOD_COMPONENTS_BY_STATE } from "./Wod.utils";
import { useWodContext } from "./WodContext";

import styles from "./WodWrapper.module.css";

const WodWrapper = (): ReactElement => {
  const { step, currentWod, currentProgram } = useWodContext();

  return (
    <IonContent
      fullscreen
      className={`main ion-padding ${styles.wodContent}`}
      style={{
        backgroundImage: hasValue(currentProgram?.backgroundImage)
          ? "url(assets/" + currentProgram.backgroundImage + ")"
          : "url(assets/wod-bg.png)",
      }}
    >
      <Title>WOD</Title>
      <Subtitle>{currentWod?.name}</Subtitle>
      {WOD_COMPONENTS_BY_STATE[step]}
    </IonContent>
  );
};

export default WodWrapper;
