import React, { type ReactElement } from "react";

import { WodProvider } from "./WodContext";
import WodWrapper from "./WodWrapper";

const WodComp = (): ReactElement => (
  <WodProvider>
    <WodWrapper />
  </WodProvider>
);

export default WodComp;
