import { createSelector } from "@reduxjs/toolkit";
import React, {
  type PropsWithChildren,
  type ReactElement,
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { useSelector } from "react-redux";

import { type PerformedWod } from "../../interfaces/performedWod";
import { type Program } from "../../interfaces/program";
import { type Progress } from "../../interfaces/progress";
import { type AllExercises, type Wod } from "../../interfaces/wod";
import { selectors } from "../../redux/selectors";
import { updatePerformedWod } from "../../utils/performedWod";
import { hasValue } from "../../utils/typescript";

export type WodStep = "workingOut" | "resting" | "ending";

export type WodContextType = {
  step: WodStep;
  setStep: (step: WodStep, performedReps?: number, load?: number) => void;
  currentSet: number;
  setCurrentSet: (set: number) => void;
  currentExercise: AllExercises | undefined;
  setCurrentExercise: (exercise: AllExercises | undefined) => void;
  currentWod: Wod | undefined;
  setCurrentWod: (wod: Wod) => void;
  performedWod: PerformedWod | undefined;
  setPerformedWod: (performedWod: PerformedWod) => void;
  initContext: (progress: Progress | null) => void;
  currentProgram: Program | undefined;
};

export const WodContext = createContext<WodContextType>({
  step: "workingOut",
  setStep: () => {},
  currentSet: 0,
  setCurrentSet: () => {},
  currentWod: undefined,
  setCurrentWod: () => {},
  currentExercise: undefined,
  setCurrentExercise: () => {},
  performedWod: undefined,
  setPerformedWod: () => {},
  initContext: () => {},
  currentProgram: undefined,
});

const selectWodProviderData = createSelector(
  selectors.progress.selectCurrentProgress,
  selectors.programs.selectAll,
  (progress, programs) => ({
    progress,
    programs,
  }),
);

export const WodProvider = ({ children }: PropsWithChildren): ReactElement => {
  const { progress, programs } = useSelector(selectWodProviderData);

  const [currentWod, setCurrentWod] = useState<Wod | undefined>();
  const [currentExercise, setCurrentExercise] = useState<
    AllExercises | undefined
  >();
  const [currentSet, setCurrentSet] = useState(0);
  const [step, setStep] = useState<WodStep>("workingOut");
  const [performedWod, setPerformedWod] = useState<PerformedWod>({
    exercises: [],
  });
  const [currentProgram, setCurrentProgram] = useState<Program | undefined>();

  const handleSetStep = useCallback(
    (step: WodStep, performedReps?: number, load?: number): void => {
      if (
        step === "resting" &&
        hasValue(currentExercise) &&
        hasValue(performedReps) &&
        hasValue(load)
      ) {
        const updatedPerformedWod = updatePerformedWod(
          performedWod,
          currentExercise,
          performedReps,
          load,
          currentSet,
        );
        setPerformedWod(updatedPerformedWod);
      }
      setStep(step);
    },
    [currentExercise, currentSet, performedWod],
  );

  const initContext = useCallback(() => {}, []);

  const wodContext: WodContextType = {
    step,
    setStep: handleSetStep,
    currentSet,
    setCurrentSet,
    currentWod,
    setCurrentWod,
    currentExercise,
    setCurrentExercise,
    performedWod,
    setPerformedWod,
    initContext,
    currentProgram,
  };

  useEffect(() => {
    if (hasValue(progress)) {
      const currentProgram = programs.find(
        (it) => it.id === progress.currentProgramId,
      );

      const currentWod = currentProgram?.schedule.find(
        (it) => it.id === progress.currentWodId,
      );
      const currentExercise = currentWod?.exercises.find(
        (it) => it.id === progress.currentExerciseId,
      );

      setCurrentProgram(currentProgram);
      setCurrentWod(currentWod);
      setCurrentExercise(currentExercise);
    }
  }, [programs, progress]);

  return (
    <WodContext.Provider value={wodContext}>{children}</WodContext.Provider>
  );
};

export const useWodContext = (): WodContextType => {
  const context = useContext(WodContext);
  if (!context) {
    throw new Error(
      "You're trying to use a wod context without being a children of a wod context provider.",
    );
  } else {
    return context;
  }
};
