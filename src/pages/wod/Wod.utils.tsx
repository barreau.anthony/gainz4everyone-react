import { type ReactElement } from "react";

import { type WodStep } from "./WodContext";
import Ending from "./steps/Ending";
import Resting from "./steps/Resting";
import WorkingOut from "./steps/WorkingOut";

export const WOD_COMPONENTS_BY_STATE: Record<WodStep, ReactElement> = {
  workingOut: <WorkingOut />,
  resting: <Resting />,
  ending: <Ending />,
};
