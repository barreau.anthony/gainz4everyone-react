import { IonButton } from "@ionic/react";
import React, { type ReactElement, useCallback } from "react";

import Card from "../../../../components/card";
import styles from "../../CommonSteps.module.css";
import { useWodContext } from "../../WodContext";

const Resting = (): ReactElement => {
  const { setStep } = useWodContext();

  const handleClickWork = useCallback(() => {

    setStep("workingOut");
  }, [setStep]);

  return (
    <>
      <Card>Resting</Card>
      <div className={styles.actionBar}>
        <IonButton onClick={handleClickWork} color="primary">
          Workout
        </IonButton>
      </div>
    </>
  );
};

export default Resting;
