import { type AllExercises } from "../../../../interfaces/wod";

export const getDescription = (
  setIndex: number,
  exercise: AllExercises,
): string => {
  const set = exercise.sets[setIndex];
  const hasWarmupSet = exercise.sets.some((it) => it.type === "warmup");
  const formattedSetIndex = hasWarmupSet ? setIndex : setIndex + 1;
  const numberOfSets = hasWarmupSet
    ? exercise.sets.length - 1
    : exercise.sets.length;

  if (set.type === "warmup") {
    return `Warmup: ${set.description}`;
  } else if (set.type === "rpe") {
    return `${set.reps} reps @ RPE ${set.rpe} (set ${formattedSetIndex}/${numberOfSets})`;
  } else {
    return `${set.reps} reps @ ${set.load}kgs`;
  }
};
