import { IonPage } from "@ionic/react";
import { Formik } from "formik";
import React, { type ReactElement, useMemo } from "react";

import FormContent from "./FormContent";
import { REPS_MODAL_SCHEMA, type RepsModalValues } from "./RepsModal.utils";

import type { AllExercises } from "../../../../../interfaces/wod";

export interface RepsModalProps {
  dismiss: (data: RepsModalValues | null, role: string) => void;
  currentExercise: AllExercises | undefined;
  currentSet: number;
}

const RepsModal = ({
  dismiss,
  currentExercise,
  currentSet,
}: RepsModalProps): ReactElement => {
  const initialValues = useMemo(() => {
    const set = currentExercise?.sets[currentSet];
    const lastWeight = Number(localStorage.getItem("lastWeight"));

    return {
      reps:
        set?.type !== "warmup" && set?.reps !== "failure" ? set?.reps ?? 0 : 0,
      weight:
        set?.type === "tm" ? set.load : isNaN(lastWeight) ? 0 : lastWeight,
    };
  }, [currentExercise?.sets, currentSet]);

  return (
    <IonPage>
      <Formik
        initialValues={initialValues}
        validationSchema={REPS_MODAL_SCHEMA}
        onSubmit={(values) => dismiss(values, "submit")}
      >
        <FormContent dismiss={dismiss} />
      </Formik>
    </IonPage>
  );
};

export default RepsModal;
