import { number, object } from "yup";

export type RepsModalValues = {
  reps: number;
  weight: number;
};

export const REPS_MODAL_SCHEMA = object().shape({
  reps: number().positive("The number of reps must be positive"),
  weight: number().positive("The weight must be positive"),
});
