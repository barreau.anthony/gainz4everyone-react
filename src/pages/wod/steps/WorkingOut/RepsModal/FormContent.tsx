import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonToolbar,
} from "@ionic/react";
import { useFormikContext } from "formik";
import React, { type ReactElement } from "react";

import FormInput from "../../../../../components/forms/FormInput";

import styles from "./FormContent.module.css";

import type { RepsModalValues } from "./RepsModal.utils";

export interface FormContentProps {
  dismiss: (data: RepsModalValues | null, role: string) => void;
}

const FormContent = ({ dismiss }: FormContentProps): ReactElement => {
  const { submitForm } = useFormikContext();
  return (
    <>
      <IonHeader>
        <IonToolbar className={styles.toolbar}>
          <IonButtons slot="end">
            <IonButton onClick={() => submitForm()}>Save</IonButton>
          </IonButtons>
          <IonButtons slot="start">
            <IonButton onClick={() => dismiss(null, "cancel")}>
              Cancel
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <p>How many reps did you do?</p>
        <FormInput name="reps" label="Reps" labelPlacement="floating" />
        <FormInput name="weight" label="Weight" labelPlacement="floating" />
      </IonContent>
    </>
  );
};

export default FormContent;
