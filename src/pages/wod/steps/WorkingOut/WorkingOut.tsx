import { IonButton, IonSpinner, useIonModal } from "@ionic/react";
import { type OverlayEventDetail } from "@ionic/react/dist/types/components/react-component-lib/interfaces";
import { ZonedDateTime } from "@js-joda/core";
import React, { type ReactElement, useCallback } from "react";

import Card from "../../../../components/card";
import { useFirebaseContext } from "../../../../components/firestore/FirebaseContext";
import { insertScheduledActivity } from "../../../../redux/activity/activity.actions";
import {
  deleteProgress,
  saveProgress,
} from "../../../../redux/progress/progress.actions";
import { useAppDispatch } from "../../../../redux/store";
import { hasValue } from "../../../../utils/typescript";
import { useWodContext } from "../../WodContext";

import RepsModal from "./RepsModal";
import { getDescription } from "./WorkingOut.utils";

import styles from "./WorkintOut.module.css";
import commonStyles from "../../CommonSteps.module.css";

import type { RepsModalValues } from "./RepsModal/RepsModal.utils";

const WorkingOut = (): ReactElement => {
  const dispatch = useAppDispatch();
  const { db, currentUser } = useFirebaseContext();

  const {
    setStep,
    currentExercise,
    currentSet,
    currentWod,
    setCurrentExercise,
    setCurrentSet,
    currentProgram,
    performedWod,
  } = useWodContext();

  const endCurrentSet = useCallback(
    (reps?: number, weight?: number) => {
      if (
        hasValue(currentExercise) &&
        hasValue(currentWod) &&
        currentUser &&
        hasValue(currentProgram)
      ) {
        const hasRemainingSetsInCurrentExercise =
          currentExercise.sets.length > currentSet + 1;
        const isWodDone =
          currentWod?.exercises.length === currentExercise?.id + 1;
        const nextWodId = isWodDone
          ? currentWod?.id + 1 < currentProgram.schedule.length
            ? currentWod?.id + 1
            : undefined
          : currentWod?.id;

        setCurrentSet(hasRemainingSetsInCurrentExercise ? currentSet + 1 : 0);

        if (!hasRemainingSetsInCurrentExercise) {
          setCurrentExercise(currentWod?.exercises[currentExercise.id + 1]);

          if (hasValue(nextWodId)) {
            dispatch(
              saveProgress(
                {
                  currentProgramId: currentProgram?.id,
                  currentExerciseId: isWodDone
                    ? 0
                    : currentWod?.exercises[currentExercise.id + 1].id,
                  currentWodId: nextWodId,
                  wodStatus: "started",
                },
                currentUser,
                db,
              ),
            );
          } else {
            dispatch(deleteProgress(currentUser.uid, db));
          }

          if (isWodDone && hasValue(performedWod)) {
            dispatch(
              insertScheduledActivity(
                {
                  type: "scheduled",
                  performedWod,
                  date: ZonedDateTime.now(),
                  title: currentProgram.name,
                  description: currentWod.name,
                },
                db,
              ),
            );
            setStep("ending");
          } else {
            setStep("resting", reps, weight);
          }
        } else {
          setStep("resting", reps, weight);
        }
      }
    },
    [
      currentExercise,
      currentProgram,
      currentSet,
      currentUser,
      currentWod,
      db,
      dispatch,
      performedWod,
      setCurrentExercise,
      setCurrentSet,
      setStep,
    ],
  );

  const [presentModal, dismiss] = useIonModal(RepsModal, {
    dismiss: (data: RepsModalValues | null, role: string) =>
      dismiss(data, role),
    currentExercise,
    currentSet,
  });

  const handleOpenModal = useCallback(() => {
    presentModal({
      onWillDismiss: (ev: CustomEvent<OverlayEventDetail>) => {
        if (ev.detail.role === "submit") {
          const { reps, weight } = ev.detail.data;
          localStorage.setItem("lastWeight", weight);
          endCurrentSet(reps, weight);
        }
      },
    });
  }, [endCurrentSet, presentModal]);

  const set = currentExercise?.sets[currentSet];

  return hasValue(currentWod) && hasValue(currentExercise) && hasValue(set) ? (
    <>
      <Card>
        <span className={styles.exerciseName}>{currentExercise.name}</span>
        <p>{getDescription(currentSet, currentExercise)}</p>
      </Card>
      <div className={commonStyles.actionBar}>
        {set.type !== "warmup" ? (
          <IonButton onClick={handleOpenModal} color="primary">
            Reps done
          </IonButton>
        ) : (
          <IonButton onClick={() => endCurrentSet()} color="primary">
            Warmup done
          </IonButton>
        )}
      </div>
    </>
  ) : (
    <IonSpinner />
  );
};

export default WorkingOut;
