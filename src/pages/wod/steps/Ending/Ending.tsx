import { IonButton } from "@ionic/react";
import React, { type ReactElement, useCallback } from "react";
import { useHistory } from "react-router";

import Card from "../../../../components/card";

import styles from "../../CommonSteps.module.css";

const Ending = (): ReactElement => {
  const history = useHistory();
  const handleBackToDashboard = useCallback(() => {
    history.push("/dashboard");
  }, [history]);

  return (
    <>
      <Card>End of your WOD! Congratulations!</Card>
      <div className={styles.actionBar}>
        <IonButton onClick={handleBackToDashboard} color="primary">
          Go back to dashboard
        </IonButton>
      </div>
    </>
  );
};

export default Ending;
