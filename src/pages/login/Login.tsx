import {
  IonButton,
  IonContent,
  IonIcon,
  IonSpinner,
  useIonToast,
} from "@ionic/react";
import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { barbell, logoGoogle } from "ionicons/icons";
import React, { type MouseEvent, type ReactElement, useCallback } from "react";
import { Redirect } from "react-router";

import { useFirebaseContext } from "../../components/firestore/FirebaseContext";

import styles from "./Login.module.css";

const provider = new GoogleAuthProvider();

const Login = (): ReactElement => {
  const { auth, isAuthenticated, isAppReady } = useFirebaseContext();
  const [presentToast] = useIonToast();

  const handleClickOnLogin = useCallback(
    (e: MouseEvent<HTMLIonButtonElement>) => {
      e.preventDefault();
      signInWithPopup(auth, provider).catch((error) => {
        presentToast({
          message: "There was an error while authenticating.",
          color: "danger",
          duration: 3000,
        });
        console.error("There was an error while authenticating.", { error });
      });
    },
    [auth, presentToast],
  );

  return isAuthenticated ? (
    <Redirect to="/dashboard" />
  ) : (
    <IonContent fullscreen className={`main ion-padding ${styles.page}`}>
      <div className={styles.container}>
        {isAppReady ? (
          <div className={styles.wrapper}>
            <span className={styles.title}>
              <IonIcon className={styles.icon} icon={barbell} /> Gainz 4
              everyone
            </span>
            <span className={styles.punchline}>
              Your sweaty journey starts here.
            </span>
            <IonButton onClick={handleClickOnLogin}>
              <IonIcon className={styles.icon} icon={logoGoogle} />
              Login with Google
            </IonButton>
          </div>
        ) : (
          <div className={styles.wrapperSpinner}>
            <IonSpinner className={styles.spinner} />
          </div>
        )}
      </div>
    </IonContent>
  );
};

export default Login;
