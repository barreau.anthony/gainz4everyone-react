import { IonCol, IonGrid, IonIcon, IonRow } from "@ionic/react";
import { pulse } from "ionicons/icons";
import React, { type ReactElement } from "react";

import Card from "../../../components/card";
import { type AllExercises } from "../../../interfaces/wod";
import {
  extractRepsFromExercise,
  extractWeightsFromExercise,
  getSetsWithoutWarmup,
  isExerciseWithVariableReps,
  isExerciseWithVariableWeights,
} from "../../../utils/exercises";

import styles from "./Exercise.module.css";

export interface ExerciseProps {
  exercise: AllExercises;
}

const Exercise = ({ exercise }: ExerciseProps): ReactElement => (
  <Card key={`exercise-${exercise.id}`} size="small">
    <IonGrid>
      <IonRow>
        <IonCol className={styles.col}>
          <span className={styles.colHeader}></span>
          {exercise.name}
        </IonCol>
        <IonCol className={styles.col}>
          <span className={styles.colHeader}>SETS</span>
          {getSetsWithoutWarmup(exercise.sets).length}
        </IonCol>
        <IonCol className={styles.col}>
          <span className={styles.colHeader}>REPS</span>
          {isExerciseWithVariableReps(exercise) ? (
            <IonIcon name={pulse}></IonIcon>
          ) : (
            <span>{extractRepsFromExercise(exercise)}</span>
          )}
        </IonCol>
        <IonCol className={styles.col}>
          <span className={styles.colHeader}>WEIGHT</span>
          {isExerciseWithVariableWeights(exercise) ? (
            <IonIcon name={pulse}></IonIcon>
          ) : (
            <span>{extractWeightsFromExercise(exercise)}</span>
          )}
        </IonCol>
        <IonCol className={styles.col}>
          <span className={styles.colHeader}>REST</span>
          {exercise.rest} s
        </IonCol>
      </IonRow>
    </IonGrid>
  </Card>
);

export default Exercise;
