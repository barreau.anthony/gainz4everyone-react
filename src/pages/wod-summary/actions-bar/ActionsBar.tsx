import { IonButton, IonIcon, useIonAlert } from "@ionic/react";
import { play, stop } from "ionicons/icons";
import React, { type ReactElement, useCallback } from "react";
import { useHistory } from "react-router";

import { useFirebaseContext } from "../../../components/firestore/FirebaseContext";
import { stopProgram } from "../../../redux/programs/programs.actions";
import { saveProgress } from "../../../redux/progress/progress.actions";
import { useAppDispatch } from "../../../redux/store";
import { hasValue } from "../../../utils/typescript";
import { useWodContext } from "../../wod/WodContext";

import styles from "./ActionsBar.module.css";

const ActionsBar = (): ReactElement => {
  const { currentWod, currentExercise, currentProgram } = useWodContext();
  const history = useHistory();
  const dispatch = useAppDispatch();
  const { db, currentUser } = useFirebaseContext();
  const [presentAlert] = useIonAlert();

  const handleClickOnGoToExercise = useCallback(() => {
    if (
      hasValue(currentUser) &&
      hasValue(currentWod) &&
      hasValue(currentExercise) &&
      hasValue(currentProgram)
    ) {
      dispatch(
        saveProgress(
          {
            currentWodId: currentWod?.id,
            currentExerciseId: currentExercise.id,
            wodStatus: "started",
            currentProgramId: currentProgram.id,
          },
          currentUser,
          db,
        ),
      ).then(() => history.push("/wod"));
    }
  }, [
    currentExercise,
    currentProgram,
    currentUser,
    currentWod,
    db,
    dispatch,
    history,
  ]);

  const handleClickOnStopWorkout = useCallback(() => {
    presentAlert({
      message: "Are your sure you want to stop this program?",
      buttons: [
        {
          text: "Ok",
          role: "confirm",
        },
        {
          text: "Cancel",
          role: "cancel",
        },
      ],
      onDidDismiss: ({ detail }) => {
        if (detail.role === "confirm" && hasValue(currentUser)) {
          dispatch(stopProgram(currentUser, db));
          dispatch(saveProgress(null, currentUser, db)).then(() =>
            history.replace("/dashboard"),
          );
        }
      },
    });
  }, [currentUser, db, dispatch, history, presentAlert]);

  return (
    <div className={styles.actionsContainer}>
      <IonButton
        className={styles.btn}
        color="primary"
        size="small"
        onClick={handleClickOnGoToExercise}
      >
        <IonIcon slot="start" icon={play}></IonIcon>
        Start WOD
      </IonButton>
      <IonButton
        className={styles.btn}
        color="danger"
        size="small"
        onClick={handleClickOnStopWorkout}
      >
        <IonIcon slot="start" icon={stop}></IonIcon>
        Stop program
      </IonButton>
    </div>
  );
};

export default ActionsBar;
