import React, { type ReactElement } from "react";

import { WodProvider } from "../wod/WodContext";

import WodSummaryWrapper from "./WodSummaryWrapper";

const WodSummary = (): ReactElement => (
  <WodProvider>
    <WodSummaryWrapper />
  </WodProvider>
);

export default WodSummary;
