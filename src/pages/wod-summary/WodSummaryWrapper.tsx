import { IonContent, IonList } from "@ionic/react";
import React, { type ReactElement } from "react";

import Subtitle from "../../components/subtitle";
import Title from "../../components/title";
import { hasValue } from "../../utils/typescript";
import { useWodContext } from "../wod/WodContext";

import ActionsBar from "./actions-bar";
import Exercise from "./exercise";

import styles from "./WodSummary.module.css";

const WodSummaryWrapper = (): ReactElement => {
  const { currentWod } = useWodContext();

  return (
    <IonContent
      fullscreen
      className={`main ion-padding ${styles.wodContent}`}
      style={{
        backgroundImage: hasValue(currentWod?.backgroundImage)
          ? "url(assets/" + currentWod.backgroundImage + ")"
          : "url(assets/wod-bg.png)",
      }}
    >
      <Title>WOD</Title>
      <Subtitle>{currentWod?.name}</Subtitle>
      <ActionsBar />
      <IonList className={styles.list}>
        {currentWod?.exercises.map((exercise, index) => (
          <Exercise key={`exercise-${index}`} exercise={exercise} />
        ))}
      </IonList>
      <ActionsBar />
    </IonContent>
  );
};

export default WodSummaryWrapper;
