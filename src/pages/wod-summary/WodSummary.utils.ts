import { type Program } from "../../interfaces/program";
import { type Wod } from "../../interfaces/wod";

export interface WodSummaryData {
  wod: Wod;
  backgroundImage: string | undefined;
  program: Program;
}
