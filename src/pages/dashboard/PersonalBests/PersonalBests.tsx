import { IonCol, IonGrid, IonRow, IonSpinner } from "@ionic/react";
import React, { type ReactElement } from "react";
import { useSelector } from "react-redux";

import Card from "../../../components/card";
import { selectors } from "../../../redux/selectors";
import { usePersonalBests } from "../../onerms/usePersonalBest";

import styles from "./PersonalBests.module.css";

const PersonalBests = (): ReactElement => {
  const personalBests = useSelector(selectors.personalBests.selectLatest);

  const loading = usePersonalBests();

  return (
    <Card>
      <IonGrid>
        <IonRow>
          <IonCol className={styles.pb}>DL</IonCol>
          <IonCol className={styles.pb}>OHP</IonCol>
          <IonCol className={styles.pb}>Bench</IonCol>
          <IonCol className={styles.pb}>Squat</IonCol>
        </IonRow>
        {!loading ? (
          <IonRow>
            <IonCol className={styles.pb}>
              <span className={styles.value}>
                {personalBests?.deadlift ?? "N.C"}
              </span>
            </IonCol>
            <IonCol className={styles.pb}>
              <span className={styles.value}>
                {personalBests?.ohp ?? "N.C"}
              </span>
            </IonCol>
            <IonCol className={styles.pb}>
              <span className={styles.value}>
                {personalBests?.bench ?? "N.C"}
              </span>
            </IonCol>
            <IonCol className={styles.pb}>
              <span className={styles.value}>
                {personalBests?.squat ?? "N.C"}
              </span>
            </IonCol>
          </IonRow>
        ) : (
          <IonSpinner />
        )}
      </IonGrid>
    </Card>
  );
};

export default PersonalBests;
