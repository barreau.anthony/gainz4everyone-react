import { IonButton } from "@ionic/react";
import { createSelector } from "@reduxjs/toolkit";
import { play } from "ionicons/icons";
import React, { type ReactElement, useCallback } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";

import Card from "../../../components/card";
import CircularButton from "../../../components/circular-button";
import { selectors } from "../../../redux/selectors";
import { hasValue } from "../../../utils/typescript";

import styles from "./CurrentWorkout.module.css";

const selectCurrentWorkoutData = createSelector(
  selectors.progress.selectCurrentProgress,
  selectors.programs.selectAll,
  (progress, programs) => {
    if (hasValue(progress)) {
      const currentProgram = programs.find(
        (it) => it.id === progress.currentProgramId,
      );
      const currentWod = currentProgram?.schedule.find(
        (it) => it.id === progress.currentWodId,
      );

      return {
        program: currentProgram,
        wod: currentWod,
        status: progress.wodStatus,
      };
    } else {
      return undefined;
    }
  },
);

const CurrentWorkout = (): ReactElement => {
  const currentWorkoutData = useSelector(selectCurrentWorkoutData);
  const history = useHistory();

  const handleGoToWorkoutClick = useCallback(() => {
    if (currentWorkoutData?.status === "not_started") {
      history.push("/wod-summary");
    } else {
      history.push("/wod");
    }
  }, [currentWorkoutData?.status, history]);

  const handleGoToProgramsClick = useCallback(() => {
    history.push("/programs");
  }, [history]);

  return (
    <>
      {hasValue(currentWorkoutData) &&
      hasValue(currentWorkoutData.program) &&
      hasValue(currentWorkoutData.wod) ? (
        <Card
          className={styles.currentWorkout}
          backgroundImage={currentWorkoutData.program.backgroundImage}
        >
          <div className={styles.title}>
            {currentWorkoutData.program.name} by{" "}
            {currentWorkoutData.program.author}
          </div>
          <h4 className={styles.subtitle}>{currentWorkoutData.wod.name}</h4>
          <CircularButton
            icon={play}
            onClick={handleGoToWorkoutClick}
          ></CircularButton>
        </Card>
      ) : (
        <Card className={styles.currentWorkout}>
          <p>
            You have no planned workout. Pick one from our available programs!
          </p>
          <IonButton onClick={handleGoToProgramsClick}>
            Browse programs
          </IonButton>
        </Card>
      )}
    </>
  );
};

export default CurrentWorkout;
