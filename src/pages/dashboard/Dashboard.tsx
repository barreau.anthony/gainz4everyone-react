import { IonContent, IonSpinner } from "@ionic/react";
import React, { type ReactElement } from "react";

import Subtitle from "../../components/subtitle";
import Title from "../../components/title";
import { useProgress } from "../../hooks/useProgress";

import CurrentWorkout from "./CurrentWorkout";
import PersonalBests from "./PersonalBests";

const DashboardPage = (): ReactElement => {
  const loading = useProgress();
  return (
    <IonContent fullscreen className="main ion-padding">
      <Title>Welcome back!</Title>
      <Subtitle>Workout of the day</Subtitle>
      {loading ? <IonSpinner /> : <CurrentWorkout></CurrentWorkout>}

      <Subtitle>Personal bests</Subtitle>
      <PersonalBests></PersonalBests>
    </IonContent>
  );
};

export default DashboardPage;
